<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="HeaderProperty.jsp" %>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                calculate();
            });
            function calculate() {
                var price = new Array();
                var quantity = new Array();
            <c:forEach items="${values}" var="a">
                price.push("${a.price}");
                quantity.push("${a.quantity}");
            </c:forEach>
                var a = 0;

                var x = document.getElementsByClassName("total");
                var total = new Array();
                for (i = 0; i < price.length; i++) {
                    total[i] = price[i] * quantity[i];
                    x[i].innerHTML = total[i];
                    a = a + total[i];

                }


                document.getElementById("subTotal").innerHTML = a;
                var ecotax = 2;
                document.getElementById("ecotax").innerHTML = ecotax;
                var vat = a * 0.175;
                document.getElementById("vat").innerHTML = vat;
                var grandtotal = a + vat + ecotax;
                document.getElementById("grandTotal").innerHTML = grandtotal;
            }
        </script>

    </head>
    <body>		
        <div id="top-bar" class="container">
            <div class="row">
                <div class="span4">
                    <form method="POST" class="search_form">
                        <input type="text" class="input-block-level search-query" Placeholder="eg. T-sirt">
                    </form>
                </div>
                <div class="span8">
                    <div class="account pull-right">
                        <ul class="user-menu">				
                            <li><a href="#">My Account</a></li>
                            <li><a href="cart.html">Your Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>					
                            <li><a href="register.html">Login</a></li>			
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="wrapper" class="container">
            <section class="navbar main-menu">
                <div class="navbar-inner main-menu">				
                    <a href="index.html" class="logo pull-left"><img src="assets/themes/images/logo.png" class="site_logo" alt=""></a>
                    <nav id="menu" class="pull-right">
                        <ul>
                            <li><a href="./products.html">Woman</a>					
                                <ul>
                                    <li><a href="./products.html">Lacinia nibh</a></li>									
                                    <li><a href="./products.html">Eget molestie</a></li>
                                    <li><a href="./products.html">Varius purus</a></li>									
                                </ul>
                            </li>															
                            <li><a href="./products.html">Man</a></li>			
                            <li><a href="./products.html">Sport</a>
                                <ul>									
                                    <li><a href="./products.html">Gifts and Tech</a></li>
                                    <li><a href="./products.html">Ties and Hats</a></li>
                                    <li><a href="./products.html">Cold Weather</a></li>
                                </ul>
                            </li>							
                            <li><a href="./products.html">Hangbag</a></li>
                            <li><a href="./products.html">Best Seller</a></li>
                            <li><a href="./products.html">Top Seller</a></li>
                        </ul>
                    </nav>
                </div>
            </section>				
            <section class="header_text sub">
                <img class="pageBanner" src="themes/images/pageBanner.png" alt="New products" >
                <h4><span>Shopping Cart</span></h4>
            </section>
            <section class="main-content">				
                <div class="row">
                    <div class="span9">					
                        <h4 class="title"><span class="text"><strong>Your</strong> Cart</span></h4>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Remove</th>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Total</th>
                                </tr>
                            </thead>

                            <tbody>



                                <c:forEach items="${values}" var="hash">
                                    <tr id="markup">

                                        <td><input type="checkbox" value="option1"></td>
                                        <td><a href="product_detail.html"><img alt="" src="assets/Myimage/${hash.photo}"></a></td>
                                        <td >${hash.productName}</td>
                                        <td><input type="text" value="${hash.quantity}" name="quantity" class="input-mini"></td>
                                        <td class="price">${hash.price}</td>
                                        <td class="total" onload="calculate()"></td>
                                    </tr>			  
                                </c:forEach>


                            </tbody>
                        </table>
                        <h4>What would you like to do next?</h4>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                            Use Coupon Code
                        </label>
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Estimate Shipping &amp; Taxes
                        </label>
                        <hr>
                        <p class="cart-total right">
                            <strong>Sub-Total</strong>: $<span id="subTotal">0.00</span><br>
                            <strong>Eco Tax (-2.00)</strong>: $<span id="ecotax">0.00</span><br>
                            <strong>VAT (17.5%)</strong>: $<span id="vat">0.00</span><br>
                            <strong>Total</strong>: $<span id="grandTotal">0.00</span><br>
                        </p>
                        <hr/>
                        <p class="buttons center">				
                            <button class="btn" type="button">Update</button>
                            <button class="btn" type="button" onclick="Recalculate()">Continue</button>
                            <a href="PayByPayPal" data-paypal-button="true">
                                <img src="//www.paypalobjects.com/en_US/i/btn/btn_xpressCheckout.gif" alt="Check out with PayPal" />
                            </a>
                        </p>					
                    </div>
                    <div class="span3 col">
                        <div class="block">	
                            <ul class="nav nav-list">
                                <li class="nav-header">SUB CATEGORIES</li>
                                <li><a href="products.html">Nullam semper elementum</a></li>
                                <li class="active"><a href="products.html">Phasellus ultricies</a></li>
                                <li><a href="products.html">Donec laoreet dui</a></li>
                                <li><a href="products.html">Nullam semper elementum</a></li>
                                <li><a href="products.html">Phasellus ultricies</a></li>
                                <li><a href="products.html">Donec laoreet dui</a></li>
                            </ul>
                            <br/>
                            <ul class="nav nav-list below">
                                <li class="nav-header">MANUFACTURES</li>
                                <li><a href="products.html">Adidas</a></li>
                                <li><a href="products.html">Nike</a></li>
                                <li><a href="products.html">Dunlop</a></li>
                                <li><a href="products.html">Yamaha</a></li>
                            </ul>
                        </div>
                        <div class="block">
                            <h4 class="title">
                                <span class="pull-left"><span class="text">Randomize</span></span>
                                <span class="pull-right">
                                    <a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
                                </span>
                            </h4>
                            <div id="myCarousel" class="carousel slide">
                                <div class="carousel-inner">
                                    <div class="active item">
                                        <ul class="thumbnails listing-products">
                                            <li class="span3">
                                                <div class="product-box">
                                                    <span class="sale_tag"></span>												
                                                    <a href="product_detail.html"><img alt="" src="themes/images/ladies/2.jpg"></a><br/>
                                                    <a href="product_detail.html" class="title">Fusce id molestie massa</a><br/>
                                                    <a href="#" class="category">Suspendisse aliquet</a>
                                                    <p class="price">$261</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="item">
                                        <ul class="thumbnails listing-products">
                                            <li class="span3">
                                                <div class="product-box">												
                                                    <a href="product_detail.html"><img alt="" src="themes/images/ladies/4.jpg"></a><br/>
                                                    <a href="product_detail.html" class="title">Tempor sem sodales</a><br/>
                                                    <a href="#" class="category">Urna nec lectus mollis</a>
                                                    <p class="price">$134</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>						
                    </div>
                </div>
            </section>			
            <!--BEGIN FOOTER-->
            <div id="footer">
                <div class="copyright">
                    <a href="http://themifycloud.com">2014 � KAdmin Responsive Multi-Purpose Template</a></div>
            </div>
            <!--END FOOTER-->
        </div>
        <!--END PAGE WRAPPER-->
        <script>
            function Recalculate() {
                var p = new Array();
            <c:forEach items="${values}" var="p">
                p.push(${p.price});
            </c:forEach>
                var r = document.getElementsByName("quantity");
                var q = [];
                for (var i = 0; i < r.length; i++) {
                    q.push(r[i].value);

                }
                var x = document.getElementsByClassName("total");

                var a = 0;
                var total = new Array();

                for (var j = 0; j < p.length; j++) {

                    total[j] = p[j] * q[j];

                    x[j].innerHTML = parseFloat(total[j]);
                    a = a + parseFloat(total[j]);

                }

                document.getElementById("subTotal").innerHTML = a;
                var ecotax = 2;
                document.getElementById("ecotax").innerHTML = ecotax;
                var vat = a * 0.175;
                document.getElementById("vat").innerHTML = vat;
                var grandtotal = a + vat + ecotax;
                document.getElementById("grandTotal").innerHTML = grandtotal;
            }
        </script>
        <script src="assets/adminassets/script/jquery-1.10.2.min.js"></script>
        <script src="assets/adminassets/script/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/adminassets/script/jquery-ui.js"></script>
        <script src="assets/adminassets/script/bootstrap.min.js"></script>
        <script src="assets/adminassets/script/bootstrap-hover-dropdown.js"></script>
        <script src="assets/adminassets/script/html5shiv.js"></script>
        <script src="assets/adminassets/script/respond.min.js"></script>
        <script src="assets/adminassets/script/jquery.metisMenu.js"></script>
        <script src="assets/adminassets/script/jquery.slimscroll.js"></script>
        <script src="assets/adminassets/script/jquery.cookie.js"></script>
        <script src="assets/adminassets/script/icheck.min.js"></script>
        <script src="assets/adminassets/script/custom.min.js"></script>
        <script src="assets/adminassets/script/jquery.news-ticker.js"></script>
        <script src="assets/adminassets/script/jquery.menu.js"></script>
        <script src="assets/adminassets/script/pace.min.js"></script>
        <script src="assets/adminassets/script/holder.js"></script>
        <script src="assets/adminassets/script/responsive-tabs.js"></script>
        <script src="assets/adminassets/script/jquery.flot.js"></script>
        <script src="assets/adminassets/script/jquery.flot.categories.js"></script>
        <script src="assets/adminassets/script/jquery.flot.pie.js"></script>
        <script src="assets/adminassets/script/jquery.flot.tooltip.js"></script>
        <script src="assets/adminassets/script/jquery.flot.resize.js"></script>
        <script src="assets/adminassets/script/jquery.flot.fillbetween.js"></script>
        <script src="assets/adminassets/script/jquery.flot.stack.js"></script>
        <script src="assets/adminassets/script/jquery.flot.spline.js"></script>
        <script src="assets/adminassets/script/zabuto_calendar.min.js"></script>
        <script src="assets/adminassets/script/index.js"></script>
        <!--LOADING SCRIPTS FOR CHARTS-->
        <script src="assets/adminassets/script/highcharts.js"></script>
        <script src="assets/adminassets/script/data.js"></script>
        <script src="assets/adminassets/script/drilldown.js"></script>
        <script src="assets/adminassets/script/exporting.js"></script>
        <script src="assets/adminassets/script/highcharts-more.js"></script>
        <script src="assets/adminassets/script/charts-highchart-pie.js"></script>
        <script src="assets/adminassets/script/charts-highchart-more.js"></script>
        <!--CORE JAVASCRIPT-->
        <script src="script/main.js"></script>
        <script>        (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-145464-12', 'auto');
            ga('send', 'pageview');


        </script>


    </body>
</html>