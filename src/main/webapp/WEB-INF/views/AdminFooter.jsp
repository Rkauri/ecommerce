<!--BEGIN FOOTER-->
<div id="footer">
    <div class="copyright">
        <a href="http://themifycloud.com">2014 � KAdmin Responsive Multi-Purpose Template</a></div>
</div>
<!--END FOOTER-->
</div>
<!--END PAGE WRAPPER-->
</div>
</div>
<script src="assets/adminassets/script/jquery-1.10.2.min.js"></script>
<script src="assets/adminassets/script/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/adminassets/script/jquery-ui.js"></script>
<script src="assets/adminassets/script/bootstrap.min.js"></script>
<script src="assets/adminassets/script/bootstrap-hover-dropdown.js"></script>
<script src="assets/adminassets/script/html5shiv.js"></script>
<script src="assets/adminassets/script/respond.min.js"></script>
<script src="assets/adminassets/script/jquery.metisMenu.js"></script>
<script src="assets/adminassets/script/jquery.slimscroll.js"></script>
<script src="assets/adminassets/script/jquery.cookie.js"></script>
<script src="assets/adminassets/script/icheck.min.js"></script>
<script src="assets/adminassets/script/custom.min.js"></script>
<script src="assets/adminassets/script/jquery.news-ticker.js"></script>
<script src="assets/adminassets/script/jquery.menu.js"></script>
<script src="assets/adminassets/script/pace.min.js"></script>
<script src="assets/adminassets/script/holder.js"></script>
<script src="assets/adminassets/script/responsive-tabs.js"></script>
<script src="assets/adminassets/script/jquery.flot.js"></script>
<script src="assets/adminassets/script/jquery.flot.categories.js"></script>
<script src="assets/adminassets/script/jquery.flot.pie.js"></script>
<script src="assets/adminassets/script/jquery.flot.tooltip.js"></script>
<script src="assets/adminassets/script/jquery.flot.resize.js"></script>
<script src="assets/adminassets/script/jquery.flot.fillbetween.js"></script>
<script src="assets/adminassets/script/jquery.flot.stack.js"></script>
<script src="assets/adminassets/script/jquery.flot.spline.js"></script>
<script src="assets/adminassets/script/zabuto_calendar.min.js"></script>
<script src="assets/adminassets/script/index.js"></script>
<!--LOADING SCRIPTS FOR CHARTS-->
<script src="assets/adminassets/script/highcharts.js"></script>
<script src="assets/adminassets/script/data.js"></script>
<script src="assets/adminassets/script/drilldown.js"></script>
<script src="assets/adminassets/script/exporting.js"></script>
<script src="assets/adminassets/script/highcharts-more.js"></script>
<script src="assets/adminassets/script/charts-highchart-pie.js"></script>
<script src="assets/adminassets/script/charts-highchart-more.js"></script>
<!--CORE JAVASCRIPT-->
<script src="script/main.js"></script>
