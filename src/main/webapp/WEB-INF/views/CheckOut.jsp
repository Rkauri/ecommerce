<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="HeaderProperty.jsp" %>



        <script>
            var toggle = document.getElementById("toggle");
            var content = document.getElementById("content");

            toggle.addEventListener("click", function() {
                content.classList.toggle("show");
            }, false);
        </script>

    </head>
    <body>		
        <div id="top-bar" class="container">
            <div class="row">
                <div class="span4">
                    <form method="POST" class="search_form">
                        <input type="text" class="input-block-level search-query" Placeholder="eg. T-sirt">
                    </form>
                </div>
                <div class="span8">
                    <div class="account pull-right">
                        <ul class="user-menu">				
                            <li><a href="#">My Account</a></li>
                            <li><a href="cart.html">Your Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>					
                            <li><a href="register.html">Login</a></li>			
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="wrapper" class="container">
            <section class="navbar main-menu">
                <div class="navbar-inner main-menu">				
                                     <a href="index.html" class="logo pull-left"><img src="assets/themes/images/logo.png" class="site_logo" alt=""></a>
  
                    <nav id="menu" class="pull-right">
                        <ul>
                            <li><a href="./products.html">Woman</a>					
                                <ul>
                                    <li><a href="./products.html">Lacinia nibh</a></li>									
                                    <li><a href="./products.html">Eget molestie</a></li>
                                    <li><a href="./products.html">Varius purus</a></li>									
                                </ul>
                            </li>															
                            <li><a href="./products.html">Man</a></li>			
                            <li><a href="./products.html">Sport</a>
                                <ul>									
                                    <li><a href="./products.html">Gifts and Tech</a></li>
                                    <li><a href="./products.html">Ties and Hats</a></li>
                                    <li><a href="./products.html">Cold Weather</a></li>
                                </ul>
                            </li>							
                            <li><a href="./products.html">Hangbag</a></li>
                            <li><a href="./products.html">Best Seller</a></li>
                            <li><a href="./products.html">Top Seller</a></li>
                        </ul>
                    </nav>
                </div>
            </section>				
            <section class="header_text sub">
                <img class="pageBanner" src="themes/images/pageBanner.png" alt="New products" >
                <h4><span>Check Out</span></h4>
            </section>	
            <section class="main-content">
                <div class="row">
                    <div class="span12">
                        <div class="accordion" id="accordion2">
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Checkout Options</a>
                                </div>
                                <div id="collapseOne" class="accordion-body in collapse">
                                    <div class="accordion-inner">
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <h4>New Customer</h4>
                                                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                                <form action="/Register" method="POST">
                                                    <fieldset>
                                                        <label class="radio" for="register">
                                                            <input type="radio" name="account" value="register" id="register" checked="checked">Register Account
                                                        </label>
                                                        <label class="radio" for="guest">
                                                            <input type="radio" name="account" value="guest" id="guest">Guest Checkout
                                                        </label>
                                                        <br>
                                                        <button class="btn btn-inverse" type="submit">Continue</button>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="span6">
                                                <h4>Returning Customer</h4>
                                                <p>I am a returning customer</p>
                                                <form action="Login" method="POST">
                                                    <fieldset>
                                                        <div class="control-group">
                                                            <label class="control-label">Username</label>
                                                            <div class="controls">
                                                                <input type="text" name="username" placeholder="Enter your username" id="username" class="input-xlarge">
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label">Password</label>
                                                            <div class="controls">
                                                                <input type="password" name="password" placeholder="Enter your password" id="password" class="input-xlarge">
                                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                            </div>
                                                        </div>
                                                        <button class="btn btn-inverse" ><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"> Continue</a></button>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Account &amp; Billing Details</a>
                                </div>
                                <div id="collapseTwo" id="content" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="row-fluid">
                                            <!--<form  name="form2" action="Addcustomerdetail?${_csrf.parameterName}=${_csrf.token}" method="post">-->
                                            <div class="span6">
                                                <h4>Your Personal Details</h4>

                                                <div class="control-group">
                                                    <label class="control-label">First Name</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Last Name</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>					  
                                                <div class="control-group">
                                                    <label class="control-label">Email Address</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Telephone</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Fax</label>
                                                    <div class="controls">
                                                        <input type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="part">
                                                    <div id="googleMap" style="width:500px;height:300px;"></div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <h4>Your Address</h4>
                                                <div class="control-group">
                                                    <label class="control-label">Address</label>
                                                    <div  id="locationField" class="controls">
                                                        <input id="autocomplete" placeholder="Enter your address"
                                                               onFocus="geolocate()" type="text" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Street Number:</label>
                                                    <div class="controls">
                                                        <input type="text" id="street_number"
                                                               disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>					  
                                                <div class="control-group">
                                                    <label class="control-label"><span class="required">*</span> Route:</label>
                                                    <div class="controls">
                                                        <input type="text" id="route"
                                                               disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label">Locality:</label>
                                                    <div class="controls">
                                                        <input type="text" id="locality"
                                                               disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>	
                                                <div class="control-group">
                                                    <label class="control-label">Region:</label>
                                                    <div class="controls">
                                                        <input type="text" id="administrative_area_level_1"
                                                               disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>	
                                                <div class="control-group">
                                                    <label class="control-label">Postal Code:</label>
                                                    <div class="controls">
                                                        <input type="text" id="postal_code"
                                                               disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Locality:</label>
                                                    <div class="controls">
                                                        <input type="text" id="longitude"
                                                               disabled="true" placeholder=""  name="longitude" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Locality:</label>
                                                    <div class="controls">
                                                        <input type="text" id="latitude"
                                                               disabled="true" placeholder="" name="latitude" class="input-xlarge">
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Country:</label>
                                                    <div class="controls">
                                                        <input type="text" id="country" disabled="true" placeholder="" class="input-xlarge">
                                                    </div>
                                                </div>	
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-group">
                                <div class="accordion-heading">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Confirm Order</a>
                                </div>
                                <div id="collapseThree" class="accordion-body collapse">
                                    <div class="accordion-inner">
                                        <div class="row-fluid">
                                            <form action="conformOrder" method="POST">
                                            <div class="control-group">
                                                <label for="textarea" class="control-label">Comments</label>
                                                <div class="controls">
                                                    <textarea rows="3" name="detail" id="textarea" class="span12">${uuu}
                                                        <c:forEach items="${pp}" var="d">
                                                            ${d.productId}
                                                            ${d.quantity}
                                                        </c:forEach>
                                                    </textarea>
                                                </div>
                                            </div>		 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>							
                                                        <button class="btn btn-inverse pull-right" type="submit">Confirm order</button></a>
</form>
                                            <!--</form>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>				
                    </div>
                </div>
            </section>			
            <section id="footer-bar">
                <div class="row">
                    <div class="span3">
                        <h4>Navigation</h4>
                        <ul class="nav">
                            <li><a href="./index.html">Homepage</a></li>  
                            <li><a href="./about.html">About Us</a></li>
                            <li><a href="./contact.html">Contac Us</a></li>
                            <li><a href="./cart.html">Your Cart</a></li>
                            <li><a href="./register.html">Login</a></li>							
                        </ul>					
                    </div>
                    <div class="span4">
                        <h4>My Account</h4>
                        <ul class="nav">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Wish List</a></li>
                            <li><a href="#">Newsletter</a></li>
                        </ul>
                    </div>
                    <div class="span5">
                        <p class="logo"><img src="themes/images/logo.png" class="site_logo" alt=""></p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. the  Lorem Ipsum has been the industry's standard dummy text ever since the you.</p>
                        <br/>
                        <span class="social_icons">
                            <a class="facebook" href="#">Facebook</a>
                            <a class="twitter" href="#">Twitter</a>
                            <a class="skype" href="#">Skype</a>
                            <a class="vimeo" href="#">Vimeo</a>
                        </span>
                    </div>					
                </div>	
            </section>
            <section id="copyright">
                <span>Copyright 2013 bootstrappage template  All right reserved.</span>
            </section>
        </div>
        <script src="assets/themes/js/common.js"></script>

        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script>
                                                                   // JavaScript Document
                                                                   var placeSearch, autocomplete;
                                                                   var componentForm = {
                                                                       locality: 'short_name',
                                                                       administrative_area_level_1: 'short_name',
                                                                       country: 'long_name',
                                                                       postal_code: 'short_name',
                                                                   };
                                                                   function initAutocomplete() {
                                                                       // Create the autocomplete object, restricting the search to geographical
                                                                       // location types.
                                                                       autocomplete = new google.maps.places.Autocomplete(
                                                                               /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                                                               {types: ['geocode']});
                                                                   }
                                                                   var text = document.getElementById('autocomplete');
                                                                   function initialize() {
                                                                       var mapProp = {
                                                                           center: new google.maps.LatLng(28.394857, 84.124008),
                                                                           zoom: 9,
                                                                           mapTypeId: google.maps.MapTypeId.ROADMAP
                                                                       };
                                                                       var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                                                       var autocomplete = new google.maps.places.Autocomplete(text);
                                                                       autocomplete.bindTo('bounds', map);
                                                                       var infowindow = new google.maps.InfoWindow();
                                                                       var infowindowContent = document.getElementById('googleMap');
                                                                       infowindow.setContent(infowindowContent);

                                                                       var marker = new google.maps.Marker({
                                                                           map: map,
                                                                           dragable: true,
                                                                           anchorPoint: new google.maps.Point(0, -29)
                                                                       });
                                                                       marker.addListener('click', toggleBounce);
                                                                       function toggleBounce() {
                                                                           if (marker.getAnimation() !== null) {
                                                                               marker.setAnimation(null);
                                                                           } else {
                                                                               marker.setAnimation(google.maps.Animation.BOUNCE);
                                                                           }
                                                                       }
                                                                       autocomplete.addListener('place_changed', function() {
                                                                           infowindow.close();
                                                                           marker.setVisible(false);
                                                                           var place = autocomplete.getPlace();

                                                                           if (!place.geometry) {
                                                                               window.alert("Autocomplete's returned place contains no geometry");
                                                                               return;
                                                                           }
                                                                           // If the place has a geometry, then present it on a map.
                                                                           if (place.geometry.viewport) {
                                                                               map.fitBounds(place.geometry.viewport);
                                                                           } else {
                                                                               map.setCenter(place.geometry.location);
                                                                               map.setZoom(17);  // Why 17? Because it looks good.
                                                                           }
                                                                           marker.setIcon(/** @type {google.maps.Icon} */({
                                                                               url: place.icon,
                                                                               size: new google.maps.Size(40, 40),
                                                                               origin: new google.maps.Point(0, 0),
                                                                               anchor: new google.maps.Point(17, 34),
                                                                               scaledSize: new google.maps.Size(25, 25)
                                                                           }));
                                                                           marker.setPosition(place.geometry.location);
                                                                           marker.setVisible(true);
                                                                           for (var component in componentForm) {
                                                                               document.getElementById(component).value = '';
                                                                               document.getElementById(component).disabled = false;
                                                                           }
                                                                           // Get each component of the address from the place details
                                                                           // and fill the corresponding field on the form.
                                                                           for (var i = 0; i < place.address_components.length; i++) {
                                                                               var addressType = place.address_components[i].types[0];
                                                                               if (componentForm[addressType]) {
                                                                                   var val = place.address_components[i][componentForm[addressType]];

                                                                                   document.getElementById(addressType).value = val;
                                                                               }
                                                                           }
                                                                           document.form2.latitude.value = place.geometry.location.lat();
                                                                           document.form2.longitude.value = place.geometry.location.lng();
                                                                           infowindow.open(map, marker);

                                                                       });
                                                                   }


                                                                   google.maps.event.addDomListener(window, 'load', initialize);
        </script>      
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALW_VDWmh1uXikNXWH0dzyWpr3798h6hQ&libraries=places&callback=initAutocomplete"
        async defer></script>

    </body>
</html>