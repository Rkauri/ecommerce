
<%-- 
    Document   : AddUser
    Created on : Dec 13, 2016, 1:27:12 PM
    Author     : Rkauri
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="AdminHeader.jsp" %>
</head>
<body>
    <%@include file="AdminNav.jsp" %>
    <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
                data-position="right" class="navbar-default navbar-static-side">
            <div class="sidebar-collapse menu-scroll">
                <ul id="side-menu" class="nav">
                    
                     <div class="clearfix"></div>
                    <li ><a href="AdminDashboard"><i class="fa fa-tachometer fa-fw">
                        <div class="icon-bg bg-orange"></div>
                    </i><span class="menu-title">Dashboard</span></a></li>
                    
                   
                    <li class="active"><a href="AddUser"><i class="fa fa-edit fa-fw">
                        <div class="icon-bg bg-violet"></div>
                    </i><span class="menu-title">Users</span></a>
                      
                    </li>
                   
                    
                   
                      </ul>
            </div>
        </nav>
            <!--END SIDEBAR MENU-->
            <!--BEGIN CHAT FORM-->
         
            <!--END CHAT FORM-->
            <!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                          AddUser</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Add User</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">AddUser</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
            

 <div class="page-content">
 <div class="panel">
                                            <div class="panel-heading">
                                                Registration form</div>
                                            <div class="panel-body pan">
                                                <form action="SubmitAddUser" method="POST">
                                                <div class="form-body pal">
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-user"></i>
                                                            <input id="inputName" type="text" placeholder="Username" name="username" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-envelope"></i>
                                                            <input id="inputEmail" type="text" placeholder="Email address"  name="email" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputPassword" type="password" placeholder="Password" name="password" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputConfirmPassword" type="password" placeholder="Confirm Password" name="repassword" class="form-control" /></div>
                                                    </div>
                                                      <div class="form-group">
                                                        <select class="form-control" name="role">
                                                            <optgroup label="---Select Role---"> 
                                                            <option value="ROLE_ADMIN">Admin</option>
                                                            <option value="ROLE_MODITATOR">Moditator</option>
                                                            <option value="ROLE_USER">User</option>
                                                            </optgroup>
                                                        </select></div>
                                                      <div class="form-group">
                                                        <select class="form-control" name="status">
                                                            <optgroup label="----Select Status----">
                                                            <option value="1">Active</option>
                                                            <option value="2">Inactive</option>
                                                           </optgroup>
                                                        </select></div>
                                                     <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputConfirmPassword" type="text" placeholder="Address" name="address" class="form-control" /></div>
                                                    </div>
                                                  <div class="form-group">
                                                        <select class="form-control" name="gender">
                                                            <option>Gender</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Other</option>
                                                        </select></div>
                                                  
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit</button>
                                                    <div style="color:red">
                                                    <c:if test="${not empty filesuccess}">  
                                                    ${filesuccess}                                                   
                                                </c:if>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
 <%@include file="AdminFooter.jsp" %>

  
</body>
</html>
