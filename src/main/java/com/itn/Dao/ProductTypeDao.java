/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;

import com.itn.Entity.ProductType;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class ProductTypeDao implements ProductTypeInterface{
@Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addProductType(ProductType producttype) {
    Session sess=sessionFactory.openSession();
    sess.save(producttype);
    sess.close();
    }

    @Override
    public List<ProductType> getAllProductTypeList() {
    Session sess= sessionFactory.openSession();
    List<ProductType> ptlist=sess.createCriteria(ProductType.class).list();
    sess.close();
    return ptlist;
    }
    public List getProductTypeById(int ptid){
    Session sess= sessionFactory.openSession();
    List producttype=(List) sess.get(ProductType.class,ptid);
    return producttype;
    }
    
}
