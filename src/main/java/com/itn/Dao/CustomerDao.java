/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;

import com.itn.Entity.Customerdetail;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class CustomerDao implements CustomerDaoInterface {
@Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addCustomerDetail(Customerdetail cd) {
    Session sess=sessionFactory.openSession();
    sess.save(cd);
    sess.close();
    }

    @Override
    public List<Customerdetail> getCustomerDetail() {
   Session sess=sessionFactory.openSession();
   List<Customerdetail> cd=sess.createCriteria(Customerdetail.class).list();
   return cd;
    }   
    
}
