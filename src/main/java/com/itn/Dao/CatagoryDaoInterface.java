/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;

import com.itn.Entity.Catagory;
import com.itn.Entity.ProductType;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public interface CatagoryDaoInterface {
    public void addCatagory(Catagory catagory);
    public List<Catagory> getCatagorylist();
   public Catagory getCatagoryByCatagoryName(int catagory);
   public List<Integer> getCommonProducttype(int catagoryid); 
}
