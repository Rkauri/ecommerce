/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;

import com.itn.Entity.Catagory;
import com.itn.Entity.ProductType;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class CatagorDao implements CatagoryDaoInterface{
    @Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addCatagory(Catagory catagory) {
    Session sess=sessionFactory.openSession();
    sess.save(catagory);
    sess.close();
    }

    @Override
    public List<Catagory> getCatagorylist() {
    Session sess=sessionFactory.openSession();
    List<Catagory> clist=sess.createCriteria(Catagory.class).list();
    sess.close();
    return clist;
    }

    @Override
    public Catagory getCatagoryByCatagoryName(int catagory) {
    Session sess=sessionFactory.openSession();
    Catagory cid=(Catagory) sess.get(Catagory.class, catagory);
     sess.close();
    return cid;
    }
 
    

    @Override
    public List<Integer> getCommonProducttype(int catagoryid) {
    Session sess=sessionFactory.openSession();
        String hql="select producttypeid from Commonproducttype where catagoryid="+catagoryid;
    Query query=sess.createQuery(hql);
    List<Integer> pid=query.list();
    return pid;
    }
}
