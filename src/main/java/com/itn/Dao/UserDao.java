/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Dao;

import com.itn.Entity.User;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class UserDao implements UserDaoInterface {

    @Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addUser(User user) {
        Session sess = sessionFactory.openSession();
        sess.save(user);
        sess.close();
    }

    public User addUser1(User user) {
        Session sess = sessionFactory.openSession();
        sess.save(user);
       
        System.out.println("dao ma aayo");
       // sess.close();
       return user;
    }
    @Override
    public List<User> getUser() {
        Session sess = sessionFactory.openSession();
        List<User> ulist = sess.createCriteria(User.class).list();
        sess.close();
        return ulist;
    }

    @Override
    public Long getUserByUsername(String username) {

        long uno;
        Session sess = sessionFactory.openSession();
        String hql = "select count(uid) from User where username='" + username + "'";
        Query query = sess.createQuery(hql);
        uno = (Long) query.uniqueResult();
        System.out.println(uno);
        sess.close();
        return uno;
    }

    @Override
    public List getLogin(String username, String password) {
        Session sess = sessionFactory.openSession();
        String hql = "from User where username='" + username + "' and password='" + password + "'";
        Query query = sess.createQuery(hql);
        List ulist = query.list();
        return ulist;
    }

    public int getUserIdByName(String username1) {
        Session sess = sessionFactory.openSession();
        int uid = 0;
        String hql = "select uid from User where username='" + username1 + "'";
        Query query = sess.createQuery(hql);
        uid = (Integer) query.uniqueResult();
        System.out.println(uid);
        sess.close();
        return uid;
    }

    public User getUserDetailById(int userid) {
        System.out.println("userdao ma aayo");
        Session sess = sessionFactory.openSession();
        User user = (User) sess.get(User.class, userid);
        return user;
    }
    public void update(User user){
    Session sess=sessionFactory.openSession();
    Transaction ts=sess.beginTransaction();
    sess.update(user);
    ts.commit();
    sess.close();
    }
    public void delete(int id){
    Session sess=sessionFactory.openSession();
    Transaction ts=sess.beginTransaction();
    User user=(User) sess.load(User.class, id);
    sess.delete(user);
    ts.commit();
    sess.close();
    }
}
