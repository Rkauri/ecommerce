/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;


import com.itn.Entity.Oder;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public interface OrderDaoInterface {
    public void add(Oder o);
}
