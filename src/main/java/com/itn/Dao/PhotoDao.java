/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Dao;

import com.itn.Entity.Photo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class PhotoDao implements PhotoDaoInterface{
@Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addPhoto(Photo photo) {
    Session sess=sessionFactory.openSession();
    sess.save(photo);
     sess.close();
    }

    @Override
    public List<Photo> getPhoto() {
  Session sess=sessionFactory.openSession();
  List<Photo> plist=sess.createCriteria(Photo.class).list();
   sess.close();
   return plist;
    }
    
}
