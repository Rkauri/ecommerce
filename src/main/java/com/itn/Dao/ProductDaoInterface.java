/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Dao;

import com.itn.Entity.Product;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public interface ProductDaoInterface {

    public void addProduct(Product product);

    public List<Product> viewProduct();

    public Product viewProductById(int productid);

    public int getCatagoryIdbyProductId(int productid);

    public List<Product> viewProductByCatagory(int catagoryid);

    public List<Product> getSpecifiedProduct(int catagoryid, int producttype);

    public void updateProduct(Product product);

    public void updateProductImage(String imagename, int productid);

    public void deleteProduct(int productid);
}
