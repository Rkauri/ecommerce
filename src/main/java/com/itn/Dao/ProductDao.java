/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Dao;

import com.itn.Entity.Product;
import java.util.List;
import org.eclipse.persistence.sessions.SessionProfiler;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Rkauri
 */
@Repository
public class ProductDao implements ProductDaoInterface {

    @Autowired
    SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addProduct(Product product) {
        Session sess = sessionFactory.openSession();
        sess.save(product);
        sess.close();
    }

    @Override
    public List<Product> viewProduct() {
        Session sess = sessionFactory.openSession();
        List<Product> plist = sess.createCriteria(Product.class).list();
        sess.close();
        return plist;
    }

    @Override
    public Product viewProductById(int productid) {
        Session sess = sessionFactory.openSession();
        Product p = (Product) sess.get(Product.class, productid);
        sess.close();
        System.out.println("yeta aayo");
        return p;
    }

    @Override
    public List<Product> viewProductByCatagory(int catagoryid) {
        Session sess = sessionFactory.openSession();
        String hql = " from Product where catagoryId=" + catagoryid;
        Query query = sess.createQuery(hql);

        List<Product> plist = query.list();
        sess.close();
        return plist;
    }

    public int getCatagoryIdbyProductId(int productid) {
        Session sess = sessionFactory.openSession();
        String hql = "select catagory_id from product where product_id=" + productid;
        Query query = sess.createQuery(hql);
        int cid = (Integer) query.uniqueResult();
        System.out.println(cid);
        sess.close();
        return cid;
    }

    @Override
    public void updateProduct(Product product) {
        Session sess = sessionFactory.openSession();
        Transaction ts = sess.beginTransaction();
        sess.update(product);
        ts.commit();
        sess.close();
    }

    @Override
    public void deleteProduct(int productid) {
        Session sess = sessionFactory.openSession();
        Transaction ts = sess.beginTransaction();
        Product p = (Product) sess.load(Product.class, productid);
       sess.delete(p);
        ts.commit();
        sess.close();
    }

    public String getPhotoName(int id) {
        Session sess = sessionFactory.openSession();
        String hql = "select photo from product where product_id=" + id;
        Query query = sess.createQuery(hql);
        String photoname = query.toString();
        System.out.println(photoname);
        return photoname;
    }

    @Override
    public List<Product> getSpecifiedProduct(int catagoryid, int producttype) {
        Session sess = sessionFactory.openSession();
        String hql = "select * from product where catagory_id= " + catagoryid + "and producttype=" + producttype;
        Query query = sess.createQuery(hql);
        List<Product> plist = query.list();
        return plist;
    }

    @Override
    public void updateProductImage(String imagename, int productid) {
        Session sess = sessionFactory.openSession();
        String hql = "update Product set photo='"+imagename+"' where product_id=" + productid;
        System.out.println(hql);
        Query query = sess.createQuery(hql);
        
        query.executeUpdate();
        sess.close();
    }
}
