/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rkauri
 */
@Entity
@Table(name = "oder", catalog = "bookstore", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Oder.findAll", query = "SELECT o FROM Oder o"),
    @NamedQuery(name = "Oder.findByOderId", query = "SELECT o FROM Oder o WHERE o.oderId = :oderId"),
    @NamedQuery(name = "Oder.findByCustomerId", query = "SELECT o FROM Oder o WHERE o.customerId = :customerId"),
    @NamedQuery(name = "Oder.findByProductId", query = "SELECT o FROM Oder o WHERE o.productId = :productId"),
    @NamedQuery(name = "Oder.findByQuantity", query = "SELECT o FROM Oder o WHERE o.quantity = :quantity"),
    @NamedQuery(name = "Oder.findBySize", query = "SELECT o FROM Oder o WHERE o.size = :size"),
    @NamedQuery(name = "Oder.findByColour", query = "SELECT o FROM Oder o WHERE o.colour = :colour"),
    @NamedQuery(name = "Oder.findByDetail", query = "SELECT o FROM Oder o WHERE o.detail = :detail")})
public class Oder implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "oder_id")
    private Integer oderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "customer_id")
    private int customerId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "product_id")
    private int productId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "size")
    private Integer size;
    @Size(max = 50)
    @Column(name = "colour")
    private String colour;
    @Size(max = 500)
    @Column(name = "detail")
    private String detail;

    public Oder() {
    }

    public Oder(Integer oderId) {
        this.oderId = oderId;
    }

    public Oder(Integer oderId, int customerId, int productId, int quantity) {
        this.oderId = oderId;
        this.customerId = customerId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Integer getOderId() {
        return oderId;
    }

    public void setOderId(Integer oderId) {
        this.oderId = oderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oderId != null ? oderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oder)) {
            return false;
        }
        Oder other = (Oder) object;
        if ((this.oderId == null && other.oderId != null) || (this.oderId != null && !this.oderId.equals(other.oderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.Entity.Oder[ oderId=" + oderId + " ]";
    }
    
}
