/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rkauri
 */
@Entity
@Table(name = "product_type", catalog = "bookstore", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductType.findAll", query = "SELECT p FROM ProductType p"),
    @NamedQuery(name = "ProductType.findByProducttypeid", query = "SELECT p FROM ProductType p WHERE p.producttypeid = :producttypeid"),
    @NamedQuery(name = "ProductType.findByProducttype", query = "SELECT p FROM ProductType p WHERE p.producttype = :producttype")})
public class ProductType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "producttypeid")
    private Integer producttypeid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "producttype")
    private String producttype;

    public ProductType() {
    }

    public ProductType(Integer producttypeid) {
        this.producttypeid = producttypeid;
    }

    public ProductType(Integer producttypeid, String producttype) {
        this.producttypeid = producttypeid;
        this.producttype = producttype;
    }

    public Integer getProducttypeid() {
        return producttypeid;
    }

    public void setProducttypeid(Integer producttypeid) {
        this.producttypeid = producttypeid;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (producttypeid != null ? producttypeid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductType)) {
            return false;
        }
        ProductType other = (ProductType) object;
        if ((this.producttypeid == null && other.producttypeid != null) || (this.producttypeid != null && !this.producttypeid.equals(other.producttypeid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.Entity.ProductType[ producttypeid=" + producttypeid + " ]";
    }
    
}
