/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rkauri
 */
@Entity
@Table(name = "catagory", catalog = "bookstore", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Catagory.findAll", query = "SELECT c FROM Catagory c"),
    @NamedQuery(name = "Catagory.findByCatagoryId", query = "SELECT c FROM Catagory c WHERE c.catagoryId = :catagoryId"),
    @NamedQuery(name = "Catagory.findByCatagory", query = "SELECT c FROM Catagory c WHERE c.catagory = :catagory")})
public class Catagory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "catagory_id")
    private Integer catagoryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "catagory")
    private String catagory;

    public Catagory(){
    }

    public Catagory(Integer catagoryId) {
        this.catagoryId = catagoryId;
    }

    public Catagory(Integer catagoryId, String catagory) {
        this.catagoryId = catagoryId;
        this.catagory = catagory;
    }

    public Integer getCatagoryId() {
        return catagoryId;
    }

    public void setCatagoryId(Integer catagoryId) {
        this.catagoryId = catagoryId;
    }

    public String getCatagory() {
        return catagory;
    }

    public void setCatagory(String catagory) {
        this.catagory = catagory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catagoryId != null ? catagoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catagory)) {
            return false;
        }
        Catagory other = (Catagory) object;
        if ((this.catagoryId == null && other.catagoryId != null) || (this.catagoryId != null && !this.catagoryId.equals(other.catagoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.Entity.Catagory[ catagoryId=" + catagoryId + " ]";
    }

}
