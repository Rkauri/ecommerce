/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Rkauri
 */
@Entity
@Table(name = "customerdetail", catalog = "bookstore", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customerdetail.findAll", query = "SELECT c FROM Customerdetail c"),
    @NamedQuery(name = "Customerdetail.findByCid", query = "SELECT c FROM Customerdetail c WHERE c.cid = :cid"),
    @NamedQuery(name = "Customerdetail.findByFirstname", query = "SELECT c FROM Customerdetail c WHERE c.firstname = :firstname"),
    @NamedQuery(name = "Customerdetail.findByLastname", query = "SELECT c FROM Customerdetail c WHERE c.lastname = :lastname"),
    @NamedQuery(name = "Customerdetail.findByEmail", query = "SELECT c FROM Customerdetail c WHERE c.email = :email"),
    @NamedQuery(name = "Customerdetail.findByUsername", query = "SELECT c FROM Customerdetail c WHERE c.username = :username"),
    @NamedQuery(name = "Customerdetail.findByPassword", query = "SELECT c FROM Customerdetail c WHERE c.password = :password"),
    @NamedQuery(name = "Customerdetail.findByTelephone", query = "SELECT c FROM Customerdetail c WHERE c.telephone = :telephone"),
    @NamedQuery(name = "Customerdetail.findByLocality", query = "SELECT c FROM Customerdetail c WHERE c.locality = :locality"),
    @NamedQuery(name = "Customerdetail.findByRegion", query = "SELECT c FROM Customerdetail c WHERE c.region = :region"),
    @NamedQuery(name = "Customerdetail.findByPostalcode", query = "SELECT c FROM Customerdetail c WHERE c.postalcode = :postalcode"),
    @NamedQuery(name = "Customerdetail.findByCountry", query = "SELECT c FROM Customerdetail c WHERE c.country = :country"),
    @NamedQuery(name = "Customerdetail.findByLat", query = "SELECT c FROM Customerdetail c WHERE c.lat = :lat"),
    @NamedQuery(name = "Customerdetail.findByLog", query = "SELECT c FROM Customerdetail c WHERE c.log = :log")})
public class Customerdetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cid")
    private Integer cid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "firstname")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "lastname")
    private String lastname;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "username")
    private String username;
    @Size(max = 255)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telephone")
    private String telephone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "locality")
    private String locality;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "region")
    private String region;
    @Basic(optional = false)
    @NotNull
    @Column(name = "postalcode")
    private int postalcode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lat")
    private double lat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "log")
    private double log;

    public Customerdetail() {
    }

    public Customerdetail(Integer cid) {
        this.cid = cid;
    }

    public Customerdetail(Integer cid, String firstname, String lastname, String email, String telephone, String locality, String region, int postalcode, String country, double lat, double log) {
        this.cid = cid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.telephone = telephone;
        this.locality = locality;
        this.region = region;
        this.postalcode = postalcode;
        this.country = country;
        this.lat = lat;
        this.log = log;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(int postalcode) {
        this.postalcode = postalcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cid != null ? cid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customerdetail)) {
            return false;
        }
        Customerdetail other = (Customerdetail) object;
        if ((this.cid == null && other.cid != null) || (this.cid != null && !this.cid.equals(other.cid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.itn.Entity.Customerdetail[ cid=" + cid + " ]";
    }
    
}
