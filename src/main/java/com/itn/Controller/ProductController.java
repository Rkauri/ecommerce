/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Controller;

import com.itn.Entity.Catagory;
import com.itn.Entity.Product;
import com.itn.Entity.ProductType;
import com.itn.Service.CatagoryService;
import com.itn.Service.ProductService;
import com.itn.Service.ProductTypeService;
import com.paypal.api.payments.*;
import com.paypal.api.payments.util.ResultPrinter;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rkauri
 */
@Controller
public class ProductController {

    @Autowired
    ProductService productService;
    @Autowired
    CatagoryService catagoryService;
    @Autowired
    ProductTypeService producttypeService;

    private static final String UPLOAD_DIRECTORY = "assets/Myimage";
    int productid = 0;
    private static ArrayList<Product> a = new ArrayList();
    private static String photoname;
    private static HashMap<Integer, Integer> hmap = new HashMap<Integer, Integer>();
    // Replace with your application client ID and secret
    String clientId = "ARhtWXGqYoQGyIanF-JSdQjqslsVXYFHjYQjtDoLBb8_IctGbtF9Zh0j56de_YSmw1q_zSnMpeER3VVx";
    String clientSecret = "EDvxViqkwEdra0HhewOYoIrV_5We0YAvRiD1vKpB8k7E0jGrRESD3RnWcXzoYTJwilJNfVBXW3F40D7S";

    APIContext context = new APIContext(clientId, clientSecret, "sandbox");

    public CatagoryService getCatagoryService() {
        return catagoryService;
    }

    public void setCatagoryService(CatagoryService catagoryService) {
        this.catagoryService = catagoryService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public ProductTypeService getProducttypeService() {
        return producttypeService;
    }

    public void setProducttypeService(ProductTypeService producttypeService) {
        this.producttypeService = producttypeService;
    }

    @RequestMapping(value = "/AddProduct", method = RequestMethod.GET)
    public String addProduct(ModelMap model) {

        model.addAttribute("list", catagoryService.getCatagorylist());
        model.addAttribute("ptlist", producttypeService.getAllProductTypeList());
        return "AddProduct";
    }

    @RequestMapping(value = "/DisplayProduct", method = RequestMethod.GET)
    public String displayProduct(ModelMap model) {
        List clist = productService.viewProduct();
        model.addAttribute("list", clist);
        return "Display";
    }

    @RequestMapping(value = "/SubmitAddProduct", method = RequestMethod.POST)
    public String submitAddProduct(@RequestParam("productname") String productname,
            @RequestParam("price") double price,
            @RequestParam("quantity") int quantity,
            @RequestParam("brand") String brand,
            @RequestParam("color") String color,
            @RequestParam("size") int size,
            @RequestParam("description") String description,
            @RequestParam("catagory") int catagory,
            @RequestParam("producttype") int producttype,
            @RequestParam("file") CommonsMultipartFile file, HttpServletRequest request, ModelMap model) throws FileNotFoundException, IOException {

        String filePath = request.getServletContext().getRealPath("/") + "/assets/Myimage";
        // file.transferTo(new File(filePath + File.separator+file.getOriginalFilename()));
        String filename = file.getOriginalFilename();
        filePath = filePath.replaceAll("\\\\", "/");

        System.out.println(filePath + "/" + filename);

        byte[] bytes = file.getBytes();
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                new File(filePath + File.separator + filename)));
        stream.write(bytes);
        stream.flush();
        stream.close();
        String fullpath = filename;
        Product product = new Product();
        product.setProductName(productname);
        product.setProductDescription(description);
        product.setPrice(price);
        product.setQuantity(quantity);
        product.setSize(size);
        product.setBrand(brand);
        product.setPhoto(fullpath);
        product.setColor(color);
        Catagory c = new Catagory();
        System.out.println(catagory);
        c.setCatagoryId(catagory);
        product.setCatagoryId(c);
        ProductType p = new ProductType();
        p.setProducttypeid(producttype);
        product.setProducttype(p);
        System.out.println(product.getCatagoryId());
        System.out.println(product.getProductName());
        System.out.println(product.getProductDescription());
        productService.addProduct(product);

        model.addAttribute("filesuccess", "You successed");
        return "redirect:AddProduct";
    }

    @RequestMapping(value = "/Dashboard", method = RequestMethod.GET)
    public String Dashboard(ModelMap model) {
        List plist = productService.viewProduct();
        model.addAttribute("clist", catagoryService.getCatagorylist());
        model.addAttribute("list", plist);

        return "Dashboard";
    }

    @RequestMapping(value = "/Catagory", method = RequestMethod.GET)
    public String openCatagory(@RequestParam("catagoryid") int catagoryid, ModelMap model) {
        System.out.println(catagoryid);
        model.addAttribute("clist", catagoryService.getCatagorylist());
        model.addAttribute("plist", productService.viewProductByCatagory(catagoryid));
        return "Catagory";
    }

    @RequestMapping(value = "/Product_detail", method = RequestMethod.GET)
    public String productDetail(@RequestParam("product_id") int productId, ModelMap model) {

        Product p = productService.viewProductById(productId);

        model.addAttribute("product", p);
        return "Product_detail";
    }

    @RequestMapping(value = "/addToCart", method = RequestMethod.POST)
    public String addToCart(@RequestParam("quantity") int quantity,
            @RequestParam("product_id") int productId, HttpServletRequest request, ModelMap model) {
//       HashMap<Integer,Integer> hmap = new HashMap<Integer,Integer>();
//      hmap.put(productId, quantity);
        productid = productId;
        Product p = productService.viewProductById(productId);
        p.setQuantity(quantity);
        a.add(p);

        HttpSession session = request.getSession(false);
        session.setAttribute("values", a);

        model.addAttribute("msg", "successfully added to cart");
        model.addAttribute("product_id", productId);
        List plist = productService.viewProduct();
        model.addAttribute("list", plist);
        return "redirect:Product_detail";
    }

    @RequestMapping(value = "/Cart", method = RequestMethod.GET)
    public String openCart(HttpServletRequest request, ModelMap model) {
        HttpSession session = request.getSession(false);

        // session.getAttributeNames();
        // System.out.println(a.get(0)+" "+a.get(1));
//  HashMap hh= new HashMap();
//  hh.put("xyz",session.getAttributeNames());
        //  ArrayList b= (ArrayList) session.getAttributeNames();
        // model.addAttribute("detail", a);
        //model.addAttribute("quantity", quantity);
        return "Cart";
    }

    @RequestMapping(value = "/CheckOut", method = RequestMethod.GET)
    public String checkOUt() {
        return "CheckOut";
    }

    @RequestMapping(value = "/DemoPhotoUpload", method = RequestMethod.GET)
    public String demo() {
        return "DemoPhotoUpload";
    }

    @RequestMapping(value = "/EditProduct", method = RequestMethod.GET)
    public String editProduct(@RequestParam("productId") int id, ModelMap model) {
        System.out.println(id);

        model.addAttribute("product", productService.viewProductById(id));
        photoname = productService.viewProductById(id).getPhoto();
        model.addAttribute("list", catagoryService.getCatagorylist());
        model.addAttribute("ptlist", producttypeService.getAllProductTypeList());

        return "EditProduct";
    }

    @RequestMapping(value = "/SubmitEditProduct", method = RequestMethod.POST)
    public String submitEditProduct(
            @RequestParam("product_id") int id,
            @RequestParam("productname") String productname,
            @RequestParam("price") double price,
            @RequestParam("quantity") int quantity,
            @RequestParam("brand") String brand,
            @RequestParam("color") String color,
            @RequestParam("size") int size,
            @RequestParam("description") String description,
            @RequestParam("catagory") int catagory,
            @RequestParam("producttype") int producttype,
            //@RequestParam("file") CommonsMultipartFile file,
            HttpServletRequest request, ModelMap model) throws FileNotFoundException, IOException {

//        System.out.println(id);
//        String filePath = request.getServletContext().getRealPath("/") + "/assets/Myimage";
//        // file.transferTo(new File(filePath + File.separator+file.getOriginalFilename()));
//        String filename = file.getOriginalFilename();
//        System.out.println(photoname);
//        System.out.println(filename);
//         if(!filename.equals(photoname)){
//        filePath = filePath.replaceAll("\\\\", "/");
//
//        System.out.println(filePath + "/" + filename);
//
//        byte[] bytes = file.getBytes();
//        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
//                new File(filePath + File.separator + filename)));
//        stream.write(bytes);
//        stream.flush();
//        stream.close();
//           photoname=filename;
//         }
//      
        Product product = new Product();
        product.setProductId(id);
        product.setProductName(productname);
        product.setProductDescription(description);
        product.setPrice(price);
        product.setQuantity(quantity);
        product.setSize(size);
        product.setBrand(brand);
        product.setPhoto(photoname);
        product.setColor(color);
        Catagory c = new Catagory();
        System.out.println(catagory);
        c.setCatagoryId(catagory);
        product.setCatagoryId(c);
        ProductType p = new ProductType();
        p.setProducttypeid(producttype);
        product.setProducttype(p);

        productService.updateProduct(product);

//        model.addAttribute("filesuccess", "You successed");
        return "redirect:Display";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/DemoAjax")
    public @ResponseBody
    String uploadFile(@RequestParam("file") CommonsMultipartFile file, @RequestParam("productId") int productid, HttpServletRequest request) throws FileNotFoundException, IOException {
        String filePath = request.getServletContext().getRealPath("/") + "/assets/Myimage";
        // file.transferTo(new File(filePath + File.separator+file.getOriginalFilename()));
        System.out.println(productid);
        String filename = file.getOriginalFilename();
        filePath = filePath.replaceAll("\\\\", "/");

        byte[] bytes = file.getBytes();
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                new File(filePath + File.separator + filename)));
        stream.write(bytes);
        stream.flush();
        stream.close();
        String path = filename;
        System.out.println(path);
        productService.updateProductImage(filename, productid);
        return "successful";

    }

    @RequestMapping(value = "/Payment")
    public String getPaymaent() {
        return "Payment";
    }

    @RequestMapping(value = "/PayByPayPal")
    public String pay(HttpServletRequest req, HttpServletResponse resp) {

// Set payer details
        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

// Set redirect URLs
        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("http://localhost:8084/mavenproject1/cancel");
        redirectUrls.setReturnUrl("http://localhost:8084/mavenproject1/process");


//item
        ItemList itemList = new ItemList();
        List<Item> items = new ArrayList<Item>();

        Item item = new Item();
        item.setName("Ground Coffee 40 oz").setQuantity("1").setCurrency("USD").setPrice("5");
        items.add(item);
         Item item2 = new Item();
        item.setName("T-Shirt").setQuantity("1").setCurrency("USD").setPrice("5");
        items.add(item2);
         itemList.setItems(items);
         
// Set payment details
        Details details = new Details();
        int price=0,total=0;
        for(Item i:items){
        price=Integer.parseInt(item.getPrice())+price;
        }
        details.setShipping("1");
        details.setSubtotal(String.valueOf(price));
        details.setTax("1");  
        total=Integer.parseInt(details.getShipping()+Integer.parseInt(details.getSubtotal())+Integer.parseInt(details.getTax()));
// Payment amount
        Amount amount = new Amount();
        amount.setCurrency("USD");
// Total must be equal to sum of shipping, tax and subtotal.
      
        amount.setTotal(String.valueOf(total));
        
        amount.setDetails(details);
         

        
// Transaction information
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setDescription("This is the payment transaction description.");
        transaction.setItemList(itemList);
// Add transaction to a list
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

// Add payment details
        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setRedirectUrls(redirectUrls);
        payment.setTransactions(transactions);

// Create payment
        try {
            Payment createdPayment = payment.create(context);

            Iterator links = createdPayment.getLinks().iterator();
            while (links.hasNext()) {
                Links link = (Links) links.next();
                if (link.getRel().equalsIgnoreCase("approval_url")) {
                    // REDIRECT USER TO link.getHref()
                    req.setAttribute("redirectURL", link.getHref());
                }
            }
        } catch (PayPalRESTException e) {
            System.err.println(e.getDetails());
        }
        return "response";
    }

    @RequestMapping(value = "/process")
    public String afterPayment(HttpServletRequest req, ModelMap model) {
        Payment payment = new Payment();
        payment.setId(req.getParameter("paymentId"));

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(req.getParameter("PayerID"));
        try {
            Payment createdPayment = payment.execute(context, paymentExecution);
            System.out.println(createdPayment);
        } catch (PayPalRESTException e) {
            System.err.println(e.getDetails());
        }
        return "Dashboard";
    }
}
