/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Controller;

import com.itn.Entity.User;
import com.itn.Service.UserService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Rkauri
 */
@Controller
public class Demo {

    @Autowired
    UserService userservice;

    public UserService getUserservice() {
        return userservice;
    }

    public void setUserservice(UserService userservice) {
        this.userservice = userservice;
    }

//  @RequestMapping(value = "/jsonTemplate",  method = RequestMethod.GET)
//public List<User> getAllEmployeesJSON(ModelMap model) 
//{
//   // model.addAttribute("employees",userservice.getUser() );
//    return userservice.getUser();
//}   
    @RequestMapping(value = "/jsonTemplate")
    public String getAllEmployeesJSON(ModelMap model) {
        final String uri = "http://localhost:8080/mavenproject1/employees.json";

        RestTemplate restTemplate = new RestTemplate();
        String response
                = restTemplate.getForObject(uri, String.class);

        model.addAttribute("hhh", response);
        System.out.println(response);

        return "jsonTemplate";
    }

    @RequestMapping(value = "/jsonTemplate/{id}")
    public String getEmployeeById(@PathVariable("id") int id, ModelMap model) {
        String uri = "http://localhost:8080/mavenproject1/employees/"+id;
      
      
        RestTemplate restTemplate = new RestTemplate();
        User user = restTemplate.getForObject(uri, User.class);
         model.addAttribute("hhh", user);
       
        System.out.println(user.getUid());
        System.out.println(user.getUsername());
        return "jsonTemplate";
    }

    @RequestMapping(value = "/jsonTemplate/addinfo")
    public String saveEmployee() {
      
         String uri = "http://localhost:8080/mavenproject1/employees/info";
        User user=new User();
        user.setUsername("Prem");
        user.setPassword("Prem");
        user.setRole("ROLE_ADMIN");
        user.setStatus(1);
        user.setAddress("khor");
        user.setEmail("Prem@gmail.com");
        user.setGender(1);
        RestTemplate restTemplate = new RestTemplate();
         System.out.println(user.getUsername()); 
        User user1 = restTemplate.postForObject(uri, user, User.class);
             
       return "Dashboard";
    }
    @RequestMapping(value="/updateEmployee/{id}")
    public void updateEmployee(@PathVariable("id") int id){
    final String uri = "http://localhost:8080/mavenproject1/employees/{id}";
     String id1= String.valueOf(id);
    Map<String, String> params = new HashMap<String, String>();
    params.put("id", id1);
     
    User updatedEmployee = new User(2, "New Name", "Gilly", "ROLE_USER",1,"Thahachaina","jpt@gmail.com",1);
     
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.put ( uri, updatedEmployee, params);
}
    @RequestMapping(value="DeleteEmployee/{id}")
    public void deleteEmployee(@PathVariable("id") int id){
    final String uri = "http://localhost:8080/mavenproject1/employees/{id}";
      String id1= String.valueOf(id);
    Map<String, String> params = new HashMap<String, String>();
    params.put("id", id1);
     
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.delete ( uri,  params );
}
}
