/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Controller;

import com.itn.Entity.Customerdetail;
import com.itn.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Rkauri
 */
@Controller
public class Customer {
    @Autowired
   CustomerService customerService;

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    @RequestMapping(value="/Addcustomerdetail", method=RequestMethod.POST)
    public String addCustomerDetail(@ModelAttribute Customerdetail cd){
    customerService.addCustomerDetail(cd);
    return "Checkout";
    }
}
