/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Controller;



import com.itn.Entity.Photo;
import com.itn.Service.PhotoService;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rkauri
 */
@Controller
public class PhotoController {
    @Autowired
   PhotoService photoService;
   private static final String UPLOAD_DIRECTORY = "assets/Myimage";

   
   @RequestMapping(value="/AddPhoto", method=RequestMethod.GET)
   public String addPhoto(){
   return "AddPhoto";
   }
     @RequestMapping(value = "/SubmitAddPhoto", method = RequestMethod.POST)
    public ModelAndView submitAddPhoto(@RequestParam("alt") String alt, @RequestParam("description") String description,@RequestParam("photoname") String photoname, @RequestParam("file") CommonsMultipartFile file, HttpServletRequest request) throws FileNotFoundException, IOException {

        String filePath = request.getServletContext().getRealPath("/") + "/assets/Myimage";
        // file.transferTo(new File(filePath + File.separator+file.getOriginalFilename()));
        String filename = file.getOriginalFilename();
        filePath = filePath.replaceAll("\\\\", "/");

        System.out.println(filePath + "/" + filename);

        byte[] bytes = file.getBytes();
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                new File(filePath + File.separator + filename)));
        stream.write(bytes);
        stream.flush();
        stream.close();
        String fullpath = UPLOAD_DIRECTORY + "/" + filename;
       Photo photo= new Photo();
       photo.setPhotoName(photoname);
       photo.setPhoto(fullpath);
         System.out.println(photo.getPhoto());
       photo.setAlt(alt);
       photo.setDescription(description);
       
       photoService.addPhoto(photo);
        return new ModelAndView("AddPhoto", "filesuccess", "You successed"); 
    }

}
