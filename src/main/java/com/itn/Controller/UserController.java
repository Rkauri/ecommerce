/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Controller;


import com.itn.Entity.User;
import com.itn.Service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rkauri
 */
@Controller
public class UserController {
@Autowired
    UserService userservice;

    public UserService getUserservice() {
        return userservice;
    }

    public void setUserservice(UserService userservice) {
        this.userservice = userservice;
    }
//    @RequestMapping(value="/admin",method=RequestMethod.GET)
//public String log(){
//    return "admin";
//
//}
//@RequestMapping(value="/Logout", method=RequestMethod.GET)
//public String logOut(Model model){
//SecurityContextHolder.getContext().setAuthentication(null);
//model.addAttribute("msg", "You've been logged out successfully.");   
//return "admin";
//}
//    @RequestMapping(value="/admin",method=RequestMethod.GET)
//    public String loginPage(){
//    return "admin";
//    }
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout, Model model,HttpServletRequest request) {

        
        if (error != null) {
            model.addAttribute("error", "Invalid username and password!");
           		
        }

        if (logout != null) {
            model.addAttribute("msg", "You've been logged out successfully.");
            SecurityContextHolder.getContext().setAuthentication(null);
        }   

        return "admin";

    }
    @RequestMapping(value="/AddUser", method=RequestMethod.GET)
    public String addUser(){
    return "AddUser";
    }
     @RequestMapping(value="/Signup", method=RequestMethod.GET)
    public String signUp(){
    return "Signup";
    }
     @RequestMapping(value="/SubmitSignup", method=RequestMethod.POST)
    public String submitSignup(@ModelAttribute User user,@RequestParam("repassword") String repassword,ModelMap model){
      long a=userservice.getUserByUsername(user.getUsername());
        System.out.println(a);
        if(userservice.getUserByUsername(user.getUsername())<=0){
            
        if(user.getPassword().equalsIgnoreCase(repassword)){   
            user.setRole("ROLE_USER");
            user.setStatus(1);
    userservice.addUser(user);
 model.addAttribute("filesuccess","User added successfully");
         return "admin";
      }else{
      model.addAttribute("filesuccess","Password and repassword doesn't match");
         return "Signup";
      }
//        map.put("",userService.addUser(user))
    }else{
         model.addAttribute("filesuccess","User already exists");
         return "Signup";
    
     }
    }
    @RequestMapping(value="/SubmitAddUser", method=RequestMethod.POST)
    public String submitAddUser(@ModelAttribute User user,@RequestParam("repassword") String repassword,ModelMap model){
      long a=userservice.getUserByUsername(user.getUsername());
        System.out.println(a);
        if(userservice.getUserByUsername(user.getUsername())<=0){
        if(user.getPassword().equalsIgnoreCase(repassword)){       
    userservice.addUser(user);
 model.addAttribute("filesuccess","User added successfully");
         return "AddUser";
      }else{
      model.addAttribute("filesuccess","Password and repassword doesn't match");
         return "AddUser";
      }
//        map.put("",userService.addUser(user))
    }else{
         model.addAttribute("filesuccess","User already exists");
         return "AddUser";
    
     }
    }
      @RequestMapping(value="/Register", method=RequestMethod.GET)
    public String register(){
      //  if(account.equalsIgnoreCase("register")){
        return "Register";
//        }else{
//        return "Signup";
//        }
    
    }
    @RequestMapping(value="/Login", method=RequestMethod.POST)
    public String makeLogin(@RequestParam("username") String username,@RequestParam("password") String password, ModelMap model,HttpServletRequest request){
    List ulist=userservice.getLogin(username,password);
       if(ulist.size()>0){
          model.addAttribute("msg","Login successful");
       HttpSession session=request.getSession();
       session.setAttribute("username", username);
          return "CheckOut";
       } else{
          model.addAttribute("msg","Login failed");
       return "CheckOut";
       }
    };
	
}
