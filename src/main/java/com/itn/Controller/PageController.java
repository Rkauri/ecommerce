/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Controller;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartRequest;

import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Rkauri
 */
@Controller
public class PageController {


    private static final Logger logger = LoggerFactory
            .getLogger(PageController.class);
    private static final String UPLOAD_DIRECTORY = "assets/Myimage";

  
// @RequestMapping(value = "/Demo", method = RequestMethod.GET)
//    public String addMenu1() {
//        return ("demo");
//    }
    @RequestMapping(value = "/AddPage", method = RequestMethod.GET)
    public ModelAndView addMenu() {
        return new ModelAndView("demo");
    }
//  
    @RequestMapping(value = "/ViewPage", method = RequestMethod.GET)
    public ModelAndView ViewPage() {

        return new ModelAndView("View");
    }
  @RequestMapping(value = "/DemoTinyMce", method = RequestMethod.GET)
    public ModelAndView demo() {

        return new ModelAndView("DemoTinyMce");
    }
     @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadimage(@RequestParam("file") CommonsMultipartFile file, HttpServletRequest request) throws FileNotFoundException, IOException {

        String filePath = request.getServletContext().getRealPath("/") + "/assets/Myimage";
        // file.transferTo(new File(filePath + File.separator+file.getOriginalFilename()));
        String filename = file.getOriginalFilename();
        filePath = filePath.replaceAll("\\\\", "/");

        System.out.println(filePath + "/" + filename);

        byte[] bytes = file.getBytes();
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                new File(filePath + File.separator + filename)));
        stream.write(bytes);
        stream.flush();
        stream.close();
        String fullpath = UPLOAD_DIRECTORY + "/" + filename;
       String url="<img src=\"" + fullpath + "\" />";
         System.out.println(url);
        return url;
    } 
   
    @RequestMapping(value = "/AdminDashboard", method = RequestMethod.GET)
    public String dispalyAdminDashboard() {

        return "AdminDashboard";
    }

  
}
