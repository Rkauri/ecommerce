/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Controller;

import com.itn.Entity.Oder;
import com.itn.Entity.Product;
import com.itn.Service.OrderService;
import com.itn.Service.UserService;
import java.util.ArrayList;
import javax.persistence.criteria.Order;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Rkauri
 */
@Controller
public class OrderController {
  @Autowired  
 OrderService orderService;  
@Autowired
UserService userService;

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
    @RequestMapping(value="/conformOrder",method=RequestMethod.POST)
    public String conformOrder(@RequestParam("detail")String detail,HttpServletRequest request,ModelMap model){
    HttpSession session=request.getSession(false);
   String username1=(String) session.getAttribute("username");
   ArrayList<Product> a= (ArrayList) session.getAttribute("values");
        System.out.println(a.size());
   for(int i=0; i<a.size(); i++){ 
   int uid=userService.getUserIdByName(username1);
   Oder o= new Oder();
   o.setCustomerId(uid);
   o.setProductId(a.get(i).getProductId());
   o.setQuantity(a.get(i).getQuantity());
   o.setDetail(detail);
   o.setColour("red");
   o.setSize(3);
   orderService.add(o);
       System.out.println(a.get(i).getQuantity());
 }
   model.addAttribute("uuu", username1);
  model.addAttribute("pp",a);
        System.out.println(username1);
        return "CheckOut";
    }
}
