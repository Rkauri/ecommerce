/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Controller;

import com.itn.Entity.User;
import com.itn.Service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Rkauri
 */
@RestController
public class RestTemplate {

    @Autowired
    UserService userservice;

    public UserService getUserservice() {
        return userservice;
    }

    public void setUserservice(UserService userservice) {
        this.userservice = userservice;
    }

    @RequestMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
public ResponseEntity<List<User>> listAllUsers() {
		List<User> users = userservice.getUser();
		if(users.isEmpty()){
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
//    public List<User> getAllEmployeesJSON(ModelMap model) {
//        // model.addAttribute("employees",userservice.getUser() );
//        return userservice.getUser();
//    }

//    @RequestMapping(value = "/employees/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
//    public ResponseEntity<User> getEmployeesJSONById(@PathVariable(value = "id") Integer userid) {
//        System.out.println(userid);
//        System.out.println("hello");
//        User us = userservice.getUserDetailById(userid);
//        return new ResponseEntity<User>(us, HttpStatus.OK);
//    }
@RequestMapping(value = "/employees/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> getUser(@PathVariable("id") int id) {
		System.out.println("Fetching User with id " + id);
		User user = userservice.getUserDetailById(id);
		if (user == null) {
			System.out.println("User with id " + id + " not found");
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
//    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public @ResponseBody User saveEmployeesJSON() {
//        User user= new User(0,"Rawan","rawan","ROLE_ADMIN",1,"rawan@gmail.com","sowrj",1);
//        return 
//    }
    @RequestMapping(value = "/employees/info",method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    public User createUser(@RequestBody User user) {
        
       
 
//        if (userservice.isUserExist(user)) {
//            System.out.println("A User with name " + user.getName() + " already exist");
//            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
//        }
        System.out.println("jjjjjjjj");
  
       
       //User user1=userservice.getUserDetailById(id);
       // HttpHeaders headers = new HttpHeaders();
       // headers.setLocation(ucBuilder.path("/employees/{id}").buildAndExpand(id).toUri());
        return userservice.addUser1(user);
    }
  @RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
public ResponseEntity<User> updateEmployee(@PathVariable("id") int id, @RequestBody User employee) 
{
    System.out.println(id);
    System.out.println(employee);
  userservice.update(employee);
    return new ResponseEntity<User>(employee, HttpStatus.OK);
}  
@RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE)
public ResponseEntity<String> delete(@PathVariable("id") int id) 
{
  userservice.delete(id);
    return new ResponseEntity(HttpStatus.OK);
}
}
