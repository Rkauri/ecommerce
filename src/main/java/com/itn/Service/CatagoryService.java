/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Dao.CatagorDao;
import com.itn.Entity.Catagory;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rkauri
 */
@Service
public class CatagoryService implements CatagoryServiceInterface {
@Autowired
    CatagorDao catagoryDao;

    public CatagorDao getCatagoryDao() {
        return catagoryDao;
    }

    public void setCatagoryDao(CatagorDao catagoryDao) {
        this.catagoryDao = catagoryDao;
    }

    @Override
       @Transactional
    public void addCatagory(Catagory catagory) {
    catagoryDao.addCatagory(catagory);
    }

    @Override
       @Transactional
    public List<Catagory> getCatagorylist() {
    return catagoryDao.getCatagorylist();
    }

    @Override
       @Transactional
    public Catagory getCatagoryByCatagoryName(int catagory) {
   return catagoryDao.getCatagoryByCatagoryName(catagory);
    }

    @Override
    public List<Integer> getCommonProducttype(int catagoryid) {
   return catagoryDao.getCommonProducttype(catagoryid);
    }
    
}
