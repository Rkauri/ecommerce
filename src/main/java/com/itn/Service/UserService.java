/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Service;

import com.itn.Dao.UserDao;
import com.itn.Entity.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rkauri
 */
@Service
public class UserService implements UserServiceInterface {

    @Autowired
    UserDao userdao;

    public UserDao getUserdao() {
        return userdao;
    }

    public void setUserdao(UserDao userdao) {
        this.userdao = userdao;
    }

    @Override
    @Transactional
    public void addUser(User user) {
        userdao.addUser(user);
    }

    @Override
    @Transactional
    public List<User> getUser() {
        return userdao.getUser();
    }

    @Override
    @Transactional
    public Long getUserByUsername(String username) {
        return userdao.getUserByUsername(username);
    }

    @Override
    public List getLogin(String username, String password) {
        return userdao.getLogin(username, password);
    }

    public int getUserIdByName(String username1) {
        return userdao.getUserIdByName(username1);
    }

    public User getUserDetailById(int userid) {
        System.out.println("userservice ma aayo");
        return userdao.getUserDetailById(userid);
    }
      @Transactional
    public User addUser1(User user) {
      return userdao.addUser1(user);
    }
       @Transactional
    public void update(User user) {
      userdao.update(user);
    }
      @Transactional
    public void delete(int id) {
      userdao.delete(id);
    }

}
