/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Service;

import com.itn.Entity.Catagory;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public interface CatagoryServiceInterface {

    public void addCatagory(Catagory catagory);

    public List<Catagory> getCatagorylist();

    public Catagory getCatagoryByCatagoryName(int catagory);

    public List<Integer> getCommonProducttype(int catagoryid);
}
