/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Entity.Customerdetail;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public interface CustomerServiceInterface {
      public void addCustomerDetail(Customerdetail cd);
    public List<Customerdetail> getCustomerDetail();
}
