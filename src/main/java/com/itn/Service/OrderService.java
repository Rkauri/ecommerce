/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Dao.OrderDao;
import com.itn.Entity.Oder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public class OrderService implements OrderServiceInterface{
@Autowired
    OrderDao orderDao;

    public OrderDao getOrderDao() {
        return orderDao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public void add(Oder o) {
    orderDao.add(o);
    }

    
}
