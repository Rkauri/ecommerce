/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itn.Service;

import com.itn.Dao.ProductDao;
import com.itn.Entity.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rkauri
 */
@Service
public class ProductService implements ProductServiceInterface {

    @Autowired
    ProductDao productDao;

    public ProductDao getProductDao() {
        return productDao;
    }

    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    @Transactional
    public void addProduct(Product product) {
        productDao.addProduct(product);
    }

    @Override
    @Transactional
    public List<Product> viewProduct() {
        return productDao.viewProduct();
    }

    @Override
    @Transactional
    public Product viewProductById(int productid) {
        return productDao.viewProductById(productid);
    }

    @Override
    @Transactional
    public List<Product> viewProductByCatagory(int catagoryid) {
        return productDao.viewProductByCatagory(catagoryid);
    }

    @Override
    @Transactional
    public int getCatagoryIdbyProductId(int productid) {
        return productDao.getCatagoryIdbyProductId(productid);
    }

    @Override
     @Transactional
    public void updateProduct(Product product) {
        productDao.updateProduct(product);
    }

    @Override
    public void deleteProduct(int productid) {
        productDao.deleteProduct(productid);
    }

    public String getPhotoName(int id) {
        return productDao.getPhotoName(id);
    }
    @Override
    public List<Product> getSpecifiedProduct(int catagoryid,int producttype){
    return productDao.getSpecifiedProduct(catagoryid, producttype);
    }

    @Override
    public void updateProductImage(String imagename, int productid) {
    productDao.updateProductImage(imagename, productid);
    }
}
