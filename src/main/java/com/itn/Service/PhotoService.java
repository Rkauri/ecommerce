/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Dao.PhotoDao;

import com.itn.Entity.Photo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rkauri
 */
@Service
public class PhotoService implements PhotoServiceInterface {
@Autowired
    PhotoDao photoDao;

    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    @Override
    @Transactional
    public void addPhoto(Photo photo) {
   photoDao.addPhoto(photo);
    }

    @Override
    @Transactional
    public List<Photo> getPhoto() {
   return photoDao.getPhoto();
    }

  
}
