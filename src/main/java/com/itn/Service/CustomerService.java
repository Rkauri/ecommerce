/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Dao.CustomerDao;
import com.itn.Entity.Customerdetail;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public class CustomerService implements CustomerServiceInterface {
@Autowired
    CustomerDao customerDao;

    public CustomerDao getCustomerDao() {
        return customerDao;
    }

    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }
    
    @Override
    public void addCustomerDetail(Customerdetail cd) {
    customerDao.addCustomerDetail(cd);
    }

    @Override
    public List<Customerdetail> getCustomerDetail() {
    return customerDao.getCustomerDetail();
    }
    
}
