/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;


import com.itn.Entity.User;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public interface UserServiceInterface {
    public void addUser(User user);
   public List<User> getUser();
     public Long getUserByUsername(String username);
 public List getLogin(String username,String password);

}
