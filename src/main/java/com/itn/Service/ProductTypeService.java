/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.itn.Service;

import com.itn.Dao.ProductTypeDao;
import com.itn.Entity.ProductType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rkauri
 */
@Service
public class ProductTypeService implements ProductTypeServiceInterface {
@Autowired
    ProductTypeDao producttypeDao;

    public ProductTypeDao getProducttypeDao() {
        return producttypeDao;
    }

    public void setProducttypeDao(ProductTypeDao producttypeDao) {
        this.producttypeDao = producttypeDao;
    }

    @Override
    public void addProductType(ProductType producttype) {
    producttypeDao.addProductType(producttype);
    }

    @Override
    public List<ProductType> getAllProductTypeList() {
    return producttypeDao.getAllProductTypeList();
    }
      public List getProductTypeById(int ptid){
    return  producttypeDao.getProductTypeById(ptid);
      }
}
