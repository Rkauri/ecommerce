<!DOCTYPE html>
<html>
<head>
  <script src="//cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
<!--  <script type="text/javascript">
tinymce.init({
  selector: 'textarea',
 plugins: 'image code',
  toolbar: 'undo redo | link image | code',
  // enable title field in the Image dialog
  image_title: true, 
  // enable automatic uploads of images represented by blob or data URIs
 // automatic_uploads: true,
  // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
  images_upload_path: 'assets/Myimage',
  // here we add custom filepicker only to Image dialog
 // file_picker_types: 'image', 
  // and here's our custom image picker
//  file_picker_callback: function(cb, value, meta) {
//    var input = document.createElement('input');
//    input.setAttribute('type', 'file');
//   // input.setAttribute('accept', 'assets/image/*');
//    
//    // Note: In modern browsers input[type="file"] is functional without 
//    // even adding it to the DOM, but that might not be the case in some older
//    // or quirky browsers like IE, so you might want to add it to the DOM
//    // just in case, and visually hide it. And do not forget do remove it
//    // once you do not need it anymore.
//
    input.onchange = function() {
      var file = this.files[0];
      
      // Note: Now we need to register the blob in TinyMCEs image blob
      // registry. In the next release this part hopefully won't be
      // necessary, as we are looking to handle it internally.
      var id = 'blobid' + (new Date()).getTime();
      var blobCache = tinymce.activeEditor.editorUpload.blobCache;
      var blobInfo = blobCache.create(id, file);
      blobCache.add(blobInfo);
      
      // call the callback and populate the Title field with the file name
      cb(blobInfo.blobUri(), { title: file.name });
     // cb("assets/Image",{title: file.name});
    };
    
    input.click();
  }
  </script>-->
  
</head>

<body>
  <form action="savefile" method="POST" enctype="multipart/form-data">
      <input type="text" name="menu"/><br/>
        <input type="file" id="fileUpload" name="file"><br/>

    <textarea name="description" style="height:300px" ></textarea><br/>
    <input type="submit" name="submit" value="submit"/>
  </form>
</body>
</html>
