<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="AdminHeader.jsp" %>

        <style>
 
#edit {
    position: absolute;
    top: 230px;
    left:38px;
}
.upload {
    position: absolute;
    top: 228px;
    left:120px;
}
        </style>
    </head>
    <body>
      
            
        <div>
            <!--BEGIN THEME SETTING-->

            <!--END THEME SETTING-->
            <!--BEGIN BACK TO TOP-->

            <!--END BACK TO TOP-->
            <!--BEGIN TOPBAR-->
            <%@include file="AdminNav.jsp" %>
            <!--END TOPBAR-->
            <%@include file="AdminSidebar.jsp" %>
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->


                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div class="row" style="background:#D2E4E8; color:black; min-height:850px;">

                        <div class="col-md-6">
                            <h3 style="color:red">${filesuccess}</h3>  
                            <form class="form-horizontal" action="SubmitEditProduct?${_csrf.parameterName}=${_csrf.token}" method="POST" enctype="multipart/form-data">
                                <!--<form class="form-horizontal" action="SubmitAddProduct" method="POST" enctype="multipart/form-data">-->
                                <input type="hidden" name="product_id" id="pid" value="${product.productId}"/>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu" style="text-align:left;">Product Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="${product.productName}" class="form-control" id="mn" name="productname" >
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Price:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" id="mn" name="price" value="${product.price}">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Quantity:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="quantity" value="${product.quantity}">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Brand:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="brand" value="${product.brand}">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Colour:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="color" value="${product.color}">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Catagory">Catagory:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="catagory">
                                            <optgroup label="---Select Catagory of product---"> 
                                                <c:forEach items="${list}" var="clist">
                                                    <option <c:if test="${clist.catagoryId==catagory}">selected="selected"</c:if> value="${clist.catagoryId}">${clist.catagory}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </select></div>    
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Catagory">Product Type:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="producttype">
                                            <optgroup label="---Select Catagory of product---"> 
                                                <c:forEach items="${ptlist}" var="list">
                                                    <option <c:if test="${list.producttypeid==producttypeid}">selected="selected"</c:if> value="${list.producttypeid}">${list.producttype}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </select></div>    
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Size">Size:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="size">
                                            <optgroup label="---Select size of product---"> 

                                                <option value="1">All</option>

                                            </optgroup>
                                        </select> </div>
                                </div>

                                <!--                                <input type="file" id="fileUpload" name="file" />-->

                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Description:</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="description" style="height:200px">${product.productDescription}</textarea>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-6">
                                        <button type="submit" class="btn btn-primary">Submit</button>

                                        </form>

                                    </div>
                                </div>


                        </div>
                        <div class="col-md-6" >
                            <label class="control-label col-sm-8" for="file">Featured Image:</label>
                            <div style="margin-top:10px; border:1px solid lightgoldenrodyellow; width:300px; min-height:250px;">
                            <div class="col-sm-8" id="image-holder" >  
                                <img src="assets/Myimage/${product.photo}"/>
                            </div>
                            <div class="form-group" >
                                <button class="btn-primary" id="edit">Edit Photo</button>
                                <input type="file" name="file" class="upload" id="fileUpload"/>                              
                            </div><br/>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
            <!--END CONTENT-->
            <script>
                $("#edit").click(function(){
                   // $("#image-holder").toggle('hide');
                    $("#fileUpload").toggle('show');
                });
             
            </script>
            <script>
                $("#fileUpload").on('change', function() {
                   
                    $("#fileUpload").toggle('hide');
        
                              var formData = new FormData();
                              formData.append('productId', $('#pid').val());
                                formData.append('file', $('#fileUpload')[0].files[0]);
                                
                                    console.log("form data " + formData);

                            $.ajax({
                                url: "DemoAjax", //controller path
                                data: formData, //value from form
                                type: "POST", //method type
                                   processData: false,
                                    contentType: false,
                                success: function(response) {   //function called if success
                                  alert("Image updated successfully!!!");
                                },
                                error: function(e) {            //error message
                                    alert('Error: ' + e);
                                }
                            });
            
        if (typeof (FileReader) != "undefined") {

                        var image_holder = $("#image-holder");
                        image_holder.empty();

                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);

                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[0]);
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                });</script>

            <!--BEGIN FOOTER-->
            <%@include file="AdminFooter.jsp" %>
    </body>
</html>
