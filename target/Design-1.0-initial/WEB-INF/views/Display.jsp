<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="AdminHeader.jsp" %>
    </head>
    <body>
        <div>
            <%@include file="AdminNav.jsp" %>
            <%@include file="AdminSidebar.jsp" %>                                                                                
               
                <!--BEGIN PAGE WRAPPER-->

                <div id="page-wrapper">
                    <!--BEGIN TITLE & BREADCRUMB PAGE-->
                    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                        <div class="page-header pull-left">
                            <div class="page-title">
                                Data Grid</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                            <li class="hidden"><a href="#">Data Grid</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                            <li class="active">Data Grid</li>
                        </ol>
                        <div class="clearfix">
                        </div>
                    </div>
                    <!--END TITLE & BREADCRUMB PAGE-->
                    <!--BEGIN CONTENT-->
                                        <div class="page-content">


                    <table id="example" class="table">
                        <thead>
                            <tr>
                                <th>ProductId</th>
                                <th>Product Name</th>
                                <th>Photo</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach items="${list}" var="p">
                            <tr>
                                <td>${p.productId}</td>
                                 <td>${p.productName}</td>
                                 <td><img src="assets/Myimage/${p.photo}" width="100px" height="80px"/></td>
                                 <td>${p.price}</td>
                                   <td>${p.quantity}</td>
                                   <td><textarea>${p.productDescription}</textarea></td>
                                    <td> <a href="EditProduct?productId=${p.productId}">
                                                        <button class="btn btn-xs btn-primary">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                            Edit
                                                        </button>
                                                    </a>
                                                    
                                                    <a href="DeleteProduct?productId=${p.productId}">
                                                        <button class="btn btn-xs btn-danger">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            Delete
                                                        </button>
                                                    </a>
                                                    </td>
                                
                            </tr>
                            </c:forEach>
                            
                        </tbody>
                    </table>

                    </div>
                    <!--END CONTENT-->
                    <!--BEGIN FOOTER-->
                    <div id="footer">
                        <div class="copyright">
                            <a href="http://themifycloud.com">2014 � KAdmin Responsive Multi-Purpose Template</a></div>
                    </div>
                    <!--END FOOTER-->
                </div>

                <!--END PAGE WRAPPER-->
            </div>
        </div>
        <!--<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>-->
        
        <!--<script src="assets/adminassets/script/jquery-1.10.2.min.js"></script>-->
        <script src="assets/adminassets/script/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/adminassets/script/jquery-ui.js"></script>
        <script src="assets/adminassets/script/bootstrap.min.js"></script>
        <script src="assets/adminassets/script/bootstrap-hover-dropdown.js"></script>
        <script src="assets/adminassets/script/html5shiv.js"></script>
        <script src="assets/adminassets/script/respond.min.js"></script>
        <script src="assets/adminassets/script/jquery.metisMenu.js"></script>
        <script src="assets/adminassets/script/jquery.slimscroll.js"></script>
        <script src="assets/adminassets/script/jquery.cookie.js"></script>
        <script src="assets/adminassets/script/icheck.min.js"></script>
        <script src="assets/adminassets/script/custom.min.js"></script>
        <script src="assets/adminassets/script/jquery.news-ticker.js"></script>
        <script src="assets/adminassets/script/jquery.menu.js"></script>
        <script src="assets/adminassets/script/pace.min.js"></script>
        <script src="assets/adminassets/script/holder.js"></script>
        <script src="assets/adminassets/script/responsive-tabs.js"></script>
        <script src="assets/adminassets/script/jquery.flot.js"></script>
        <script src="assets/adminassets/script/jquery.flot.categories.js"></script>
        <script src="assets/adminassets/script/jquery.flot.pie.js"></script>
        <script src="assets/adminassets/script/jquery.flot.tooltip.js"></script>
        <script src="assets/adminassets/script/jquery.flot.resize.js"></script>
        <script src="assets/adminassets/script/jquery.flot.fillbetween.js"></script>
        <script src="assets/adminassets/script/jquery.flot.stack.js"></script>
        <script src="assets/adminassets/script/jquery.flot.spline.js"></script>
        <script src="assets/adminassets/script/zabuto_calendar.min.js"></script>
        <script src="assets/adminassets/script/index.js"></script>
        <script src="assets/adminassets/script/highcharts.js"></script>
        <script src="assets/adminassets/script/data.js"></script>
        <script src="assets/adminassets/script/drilldown.js"></script>
        <script src="assets/adminassets/script/exporting.js"></script>
        <script src="assets/adminassets/script/highcharts-more.js"></script>
        <script src="assets/adminassets/script/charts-highchart-pie.js"></script>
        <script src="assets/adminassets/script/charts-highchart-more.js"></script>
        <script src="assets/adminassets/script/modernizr.min.js"></script>
        <script src="assets/adminassets/script/jplist.js"></script>

        CORE JAVASCRIPT
        <script src="assets/adminassets/script/main.js"></script>
        <script>        (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-145464-12', 'auto');
            ga('send', 'pageview');


        </script>

    </body>
</html>
