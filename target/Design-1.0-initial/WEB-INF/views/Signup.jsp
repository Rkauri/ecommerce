<%-- 
    Document   : AddUser
    Created on : Dec 13, 2016, 1:27:12 PM
    Author     : Rkauri
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forms | Forms</title>
    <meta charset="utf-8">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div>
        <!--BEGIN THEME SETTING-->
   
        <!--END THEME SETTING-->
        <!--BEGIN BACK TO TOP-->
      
        <!--END BACK TO TOP-->
        <!--BEGIN TOPBAR-->
  
        <!--END TOPBAR-->
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
         
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                        
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                    
                                    </div>
                                    <div class="col-lg-4">
                                    
                                        <div class="panel panel-orange">
                                            <div class="panel-heading">
                                                Registration form</div>
                                            <div class="panel-body pan">
                                                <form action="SubmitSignup" method="POST">
                                                <div class="form-body pal">
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-user"></i>
                                                            <input id="inputName" type="text" placeholder="Username" name="username" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-envelope"></i>
                                                            <input id="inputEmail" type="text" placeholder="Email address"  name="email" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputPassword" type="password" placeholder="Password" name="password" class="form-control" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputConfirmPassword" type="password" placeholder="Confirm Password" name="repassword" class="form-control" /></div>
                                                    </div>
                                                    <hr />
                                                     <div class="form-group">
                                                        <div class="input-icon right">
                                                            <i class="fa fa-lock"></i>
                                                            <input id="inputConfirmPassword" type="text" placeholder="Address" name="address" class="form-control" /></div>
                                                    </div>
                                                  <div class="form-group">
                                                        <select class="form-control" name="gender">
                                                            <option>Gender</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Other</option>
                                                        </select></div>
                                                  
                                                </div>
                                                <div class="form-actions text-right pal">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit</button>
                                                    <div style="color:red">
                                                    <c:if test="${not empty filesuccess}">  
                                                    ${filesuccess}                                                   
                                                </c:if>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    <div class="col-lg-4">
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                <!--BEGIN FOOTER-->
               
                <!--END FOOTER-->
            </div>
            <!--END PAGE WRAPPER-->
        </div>
    </div>
  
</body>
</html>
