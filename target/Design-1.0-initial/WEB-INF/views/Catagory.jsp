<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="HeaderProperty.jsp" %>
    </head>
    <body>		
        <div id="top-bar" class="container">
            <div class="row">
                <div class="span4">
                    <form  name="form1" class="search_form" style="color:black">
                          <input type="text" name="search" onkeyup="searchAjax();" class="input-block-level search-query "  Placeholder="eg. T-sirt">
                    </form>
                    <div id="results1">
                       
                    </div>
                </div>
 
                <div class="span8">
                    <div class="account pull-right">
                        <ul class="user-menu">				
                            <li><a href="DemoPhotoUpload">My Account</a></li>
                            <li><a href="Cart">Your Cart</a></li>
                            <li><a href="checkout.html">Checkout</a></li>					
                            <li><a href="register.html">Login</a></li>		
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="wrapper" class="container">
            <section class="navbar main-menu">
                <div class="navbar-inner main-menu">				
                    <a href="index.html" class="logo pull-left"><img src="assets/themes/images/logo.png" class="site_logo" alt=""></a>
                    <nav id="menu" class="pull-right">
                        <ul>
                            <c:forEach items="${clist}" var="list">
                                <li><a href="Catagory?catagoryid=${list.catagoryId}">${list.catagory}</a></li>	
                            </c:forEach>
<!--                            <li><a href="./products.html">Woman</a>					
                                <ul>
                                    <li><a href="./products.html">Lacinia nibh</a></li>									
                                    <li><a href="./products.html">Eget molestie</a></li>
                                    <li><a href="./products.html">Varius purus</a></li>									
                                </ul>
                            </li>															
                            <li><a href="./products.html">Man</a></li>			
                            <li><a href="./products.html">Sport</a>
                                <ul>									
                                    <li><a href="./products.html">Gifts and Tech</a></li>
                                    <li><a href="./products.html">Ties and Hats</a></li>
                                    <li><a href="./products.html">Cold Weather</a></li>
                                </ul>
                            </li>							
                            <li><a href="./products.html">Hangbag</a></li>
                            <li><a href="./products.html">Best Seller</a></li>
                            <li><a href="./products.html">Top Seller</a></li>-->
                        </ul>
                    </nav>
                </div>
            </section>
<!--            <section  class="homepage-slider" id="home-slider">
                <div class="flexslider">
                    <ul class="slides">
                             <li>
                            <img src="assets/Myimage/images/carousel/banner-1.jpg" alt="" />
                             <div class="intro">
                              
                            </div>
                        </li> 
                       
                      
                        <li>
                            <img src="assets/themes/images/carousel/banner-2.jpg" alt="" />
                            <div class="intro">
                                <h1>Mid season sale</h1>
                                <p><span>Up to 50% Off</span></p>
                                <p><span>On selected items online and in stores</span></p>
                            </div>
                        </li>
                    </ul>
                 
                </div>			
            </section>
            <section class="header_text">
                We stand for top quality templates. Our genuine developers always optimized bootstrap commercial templates. 
                <br/>Don't miss to use our cheap abd best bootstrap templates.
            </section>-->
            <section class="main-content">
                <div class="row">
                    <div class="span12">													
                        <div class="row">
                            <div class="span12">
                                <h4 class="title">
                                    <span class="pull-left"><span class="text"><span class="line">Feature <strong>Products</strong></span></span></span>
                                    <span class="pull-right">
                                        <a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
                                    </span>
                                </h4>
                                <div id="myCarousel" class="myCarousel carousel slide">
                                    <div class="carousel-inner">
                                        <div class="active item">
                                            <ul class="thumbnails">
                                                <c:forEach items="${plist}" var="p" varStatus="loop" begin="0" end="7">
                                                   
                                                <li class="span3" Style="float: left">
                                                    <div class="product-box" >
                                                        <span class="sale_tag"></span>
                                                        <p><a href="Product_detail?product_id=${p.productId}"><img class="img-responsive" src="assets/Myimage/${p.photo}" alt="" style="width:169px; height: 220px " /></a></p>
                                                        <a href="Product_detail?product_id=${p.productId}" class="title">Ut wisi enim ad</a><br/>
                                                        <a href="Product_detail?product_id=${p.productId}" class="category">Commodo consequat</a>
                                                        <p class="price">${p.price}</p>
                                                    </div>
                                                        
                                                </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="item">
                                            <ul class="thumbnails">
                                              <c:forEach items="${plist}" var="p" varStatus="loop" begin="8" end="15">
                                                   
                                                <li class="span3" Style="float: left">
                                                    <div class="product-box" >
                                                        <span class="sale_tag"></span>
                                                        <p><a href="Product_detail?product_id=${p.productId}"><img class="img-responsive" src="assets/Myimage/${p.photo}" alt="" style="width:169px; height: 220px " /></a></p>
                                                        <a href="Product_detail?product_id=${p.productId}" class="title">Ut wisi enim ad</a><br/>
                                                        <a href="Product_detail?product_id=${p.productId}" class="category">Commodo consequat</a>
                                                        <p class="price">${p.price}</p>
                                                    </div>
                                                        
                                                </li>
                                                </c:forEach>																																	
                                            </ul>
                                        </div>
                                    </div>							
                                </div>
                            </div>						
                        </div>
                        <br/>
                        <div class="row">
                            <div class="span12">
                                <h4 class="title">
                                    <span class="pull-left"><span class="text"><span class="line">Latest <strong>Products</strong></span></span></span>
                                    <span class="pull-right">
                                        <a class="left button" href="#myCarousel-2" data-slide="prev"></a><a class="right button" href="#myCarousel-2" data-slide="next"></a>
                                    </span>
                                </h4>
                                <div id="myCarousel-2" class="myCarousel carousel slide">
                                    <div class="carousel-inner">
                                        <div class="active item">
                                            <ul class="thumbnails">												
                                               <c:forEach items="${plist}" var="p" varStatus="loop" begin="16" end="23">
                                                   
                                                <li class="span3" Style="float: left">
                                                    <div class="product-box" >
                                                        <span class="sale_tag"></span>
                                                        <p><a href="Product_detail?product_id=${p.productId}"><img class="img-responsive" src="assets/Myimage/${p.photo}" alt="" style="width:169px; height: 220px " /></a></p>
                                                        <a href="Product_detail?product_id=${p.productId}" class="title">Ut wisi enim ad</a><br/>
                                                        <a href="Product_detail?product_id=${p.productId}" class="category">Commodo consequat</a>
                                                        <p class="price">${p.price}</p>
                                                    </div>
                                                        
                                                </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="item">
                                            <ul class="thumbnails">
                                               <c:forEach items="${plist}" var="p" varStatus="loop" begin="24" end="35">
                                                   
                                                <li class="span3" Style="float: left">
                                                    <div class="product-box" >
                                                        <span class="sale_tag"></span>
                                                        <p><a href="Product_detail?product_id=${p.productId}"><img class="img-responsive" src="assets/Myimage/${p.photo}" alt="" style="width:169px; height: 220px " /></a></p>
                                                        <a href="Product_detail?product_id=${p.productId}" class="title">Ut wisi enim ad</a><br/>
                                                        <a href="Product_detail?product_id=${p.productId}" class="category">Commodo consequat</a>
                                                        <p class="price">${p.price}</p>
                                                    </div>
                                                        
                                                </li>
                                                </c:forEach>																																
                                            </ul>
                                        </div>
                                    </div>							
                                </div>
                            </div>						
                        </div>
                        <div class="row feature_box">						
                            <div class="span4">
                                <div class="service">
                                    <div class="responsive">	
                                        <img src="assets/themes/images/feature_img_2.png" alt="" />
                                        <h4>MODERN <strong>DESIGN</strong></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.</p>									
                                    </div>
                                </div>
                            </div>
                            <div class="span4">	
                                <div class="service">
                                    <div class="customize">			
                                        <img src="assets/themes/images/feature_img_1.png" alt="" />
                                        <h4>FREE <strong>SHIPPING</strong></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="service">
                                    <div class="support">	
                                        <img src="assets/themes/images/feature_img_3.png" alt="" />
                                        <h4>24/7 LIVE <strong>SUPPORT</strong></h4>
                                        <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.</p>
                                    </div>
                                </div>
                            </div>	
                        </div>		
                    </div>				
                </div>
            </section>
            <section class="our_client">
                <h4 class="title"><span class="text">Manufactures</span></h4>
                <div class="row">					
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/14.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/35.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/1.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/2.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/3.png"></a>
                    </div>
                    <div class="span2">
                        <a href="#"><img alt="" src="assets/themes/images/clients/4.png"></a>
                    </div>
                </div>
            </section>
            <%@include file="Footerproperty.jsp" %>
            <script>
    function searchAjax() {
        var text =document.form1.search.value;

        var url = 'Search?name='+text;
        request = new XMLHttpRequest();
        request.onreadystatechange= function () {
            if (request.readyState == 4 && request.status == 200) {
                document.getElementById("results1").innerHTML = request.responseText;
            }
        }
        request.open("GET", url, true);
        request.send();

    }
</script>
    </body>
</html>