<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  

<!DOCTYPE html>
<html lang="en">
    <head>
      

        <%@include file="AdminHeader.jsp" %>
        <style>
            #image-holder
            {

                background-size:contain;

                display: inline-block;
            }
            .thumb-image {
                width:100%;
                position:relative;
            }
        </style>
    </head>
    <body>
        <%@include file="AdminNav.jsp" %>
        <div id="wrapper">
            <!--BEGIN SIDEBAR MENU-->
            <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
                 data-position="right" class="navbar-default navbar-static-side">
                <div class="sidebar-collapse menu-scroll">
                    <ul id="side-menu" class="nav">

                        <div class="clearfix"></div>

                        <li ><a href="AdminDashboard"><i class="fa fa-tachometer fa-fw">
                                    <div class="icon-bg bg-orange"></div>
                                </i><span class="menu-title">Dashboard</span></a></li>
                        <li class="active"><a href="ViewPage"><i class="fa fa-desktop fa-fw">
                                    <div class="icon-bg bg-pink"></div>
                                </i><span class="menu-title">Post</span></a>

                        </li>

                        <li><a href="AddUser"><i class="fa fa-edit fa-fw">
                                    <div class="icon-bg bg-violet"></div>
                                </i><span class="menu-title">Users</span></a>

                        </li>


                        <li><a href="Pages.html"><i class="fa fa-file-o fa-fw">
                                    <div class="icon-bg bg-yellow"></div>
                                </i><span class="menu-title">Pages</span></a>

                        </li>
                    </ul>
                </div>
            </nav>
            <!--END SIDEBAR MENU-->
            <!--BEGIN CHAT FORM-->

            <!--END CHAT FORM-->
            <!--BEGIN PAGE WRAPPER-->


            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Post</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Post</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Post</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>


                <div class="page-content">
                    <div class="row" style="background: #d9edf7; min-height:850px;">

                        <h3 style="color:red">${filesuccess}</h3>  
                        <form class="form-horizontal" action="savefile" method="POST" enctype="multipart/form-data">
                            <div class="form-group" style="padding-top: 10px;">
                                <label class="control-label col-sm-2" for="menu">Menu:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="mn" name="menu" placeholder="Enter menu name">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="control-label col-sm-2" for="file">Photo:</label>

                                <input type="file" id="fileUpload" name="file" placeholder="Choose Photo">

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Description:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="description" style="height:300px"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-6">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8" id="image-holder"></div>
                        <div class="col-sm-2"></div>
                    </div>
                </div>
                <div class="row" >


                    <div class="col-sm-2"></div>
                </div>
                <script>
                    $("#fileUpload").on('change', function() {

                        if (typeof (FileReader) != "undefined") {

                            var image_holder = $("#image-holder");
                            image_holder.empty();

                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $("<img />", {
                                    "src": e.target.result,
                                    "class": "thumb-image"
                                }).appendTo(image_holder);

                            }
                            image_holder.show();
                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            alert("This browser does not support FileReader.");
                        }
                    });</script>
                    <%@include file="AdminFooter.jsp" %>
                </body>
                </html>