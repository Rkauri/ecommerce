<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="AdminHeader.jsp" %>


    </head>
    <body>
        <div>
            <!--BEGIN THEME SETTING-->

            <!--END THEME SETTING-->
            <!--BEGIN BACK TO TOP-->

            <!--END BACK TO TOP-->
            <!--BEGIN TOPBAR-->
            <%@include file="AdminNav.jsp" %>
            <!--END TOPBAR-->
            <%@include file="AdminSidebar.jsp" %>
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->


                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div class="row" style="background:#D2E4E8; color:black; min-height:850px;">
                        <form class="form-horizontal" action="SubmitAddProduct?${_csrf.parameterName}=${_csrf.token}" method="POST" enctype="multipart/form-data">

                            <div class="col-md-6">
                                <h3 style="color:red">${filesuccess}</h3>  
                                <!--<form class="form-horizontal" action="SubmitAddProduct" method="POST" enctype="multipart/form-data">-->

                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu" style="text-align:left;">Product Name:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="productname" placeholder="Enter Product name">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Price:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" id="mn" name="price" placeholder="Enter price">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Quantity:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="quantity" placeholder="Enter available quantity">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Brand:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="brand" placeholder="Enter Product Brand name">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="menu">Colour:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mn" name="color" placeholder="Enter color">
                                    </div>
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Catagory">Catagory:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="catagory">
                                            <optgroup label="---Select Catagory of product---"> 
                                                <c:forEach items="${list}" var="clist">
                                                    <option value="${clist.catagoryId}">${clist.catagory}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </select></div>    
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Catagory">Product Type:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="producttype">
                                            <optgroup label="---Select Catagory of product---"> 
                                                <c:forEach items="${ptlist}" var="list">
                                                    <option value="${list.producttypeid}">${list.producttype}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </select></div>    
                                </div>
                                <div class="form-group" style="padding-top: 10px;">
                                    <label class="control-label col-sm-4" for="Size">Size:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="size">
                                            <optgroup label="---Select size of product---"> 

                                                <option value="1">All</option>

                                            </optgroup>
                                        </select> </div>
                                </div>

                               
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="pwd">Description:</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="description" style="height:200px"></textarea>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-6">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <input type="hidden"  name="${_csrf.parameterName}" value="${_csrf.token}"/> 

                                    </div>
                                </div>


                            </div>
                            <div class="col-md-6" >
                                <div class="form-group" >
                                    <label class="control-label col-sm-6" for="file">Featured Image:</label>
                                    <div class="col-sm-8">
                                        <input type="file" id="Uploadfile" name="file" placeholder="Choose Photo">
                                    </div>
                                </div>
                                <div style="margin-top:10px; background:#fff; border:1pz solid lightgray;">
                                <div class="col-sm-8" id="image-holder" style="margin-top:10px;" >  </div> 
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--END CONTENT-->
            <script>
                $("#Uploadfile").on('change', function() {

                    if (typeof (FileReader) != "undefined") {

                        var image_holder = $("#image-holder");
                        image_holder.empty();

                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);

                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[0]);
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                });</script>
            <!--BEGIN FOOTER-->
            <%@include file="AdminFooter.jsp" %>
    </body>
</html>
