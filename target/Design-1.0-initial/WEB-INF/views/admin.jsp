


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
	<head>
            <%@include file="HeaderProperty.jsp" %>
	</head>
    <body>		
		<div id="top-bar" class="container">
			<div class="row">
				<div class="span4">
					<form method="POST" class="search_form">
						<input type="text" class="input-block-level search-query" Placeholder="eg. T-sirt">
					</form>
				</div>
				<div class="span8">
					<div class="account pull-right">
						<ul class="user-menu">				
							<li><a href="#">My Account</a></li>
							<li><a href="cart.html">Your Cart</a></li>
							<li><a href="checkout.html">Checkout</a></li>					
							<li><a href="register.html">Login</a></li>		
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div id="wrapper" class="container">
			<section class="navbar main-menu">
				<div class="navbar-inner main-menu">				
					<a href="index.html" class="logo pull-left"><img src="themes/images//logo.png" class="site_logo" alt=""></a>
					<nav id="menu" class="pull-right">
						<ul>
							<li><a href="./products.html">Woman</a>					
								<ul>
									<li><a href="./products.html">Lacinia nibh</a></li>									
									<li><a href="./products.html">Eget molestie</a></li>
									<li><a href="./products.html">Varius purus</a></li>									
								</ul>
							</li>															
							<li><a href="./products.html">Man</a></li>			
							<li><a href="./products.html">Sport</a>
								<ul>									
									<li><a href="./products.html">Gifts and Tech</a></li>
									<li><a href="./products.html">Ties and Hats</a></li>
									<li><a href="./products.html">Cold Weather</a></li>
								</ul>
							</li>							
							<li><a href="./products.html">Hangbag</a></li>
							<li><a href="./products.html">Best Seller</a></li>
							<li><a href="./products.html">Top Seller</a></li>
						</ul>
					</nav>
				</div>
			</section>			
			<section class="header_text sub">
			<img class="pageBanner" src="themes/images/pageBanner.png" alt="New products" >
				<h4><span>Login or Regsiter</span></h4>
			</section>			
			<section class="main-content">				
				<div class="row">
					<div class="span5">					
						<h4 class="title"><span class="text"><strong>Login</strong> Form</span></h4>
						<form action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
							<input type="hidden" name="next" value="/">
							<fieldset>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
										<input type="text" name="j_username" placeholder="Enter your username" id="username" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Password</label>
									<div class="controls">
										<input type="password" name="j_password" placeholder="Enter your password" id="password" class="input-xlarge">
									</div>
								</div>
                                                            
								<div class="control-group">
									
									<div>
                                                                            <input type="checkbox" name="remember_me_param"  id="rememberme" ><label style="display:inline; padding:10px 0 0 10px; line-height: 10px">Remember Me</label>
									</div>
								</div>
								<div class="control-group">
									<input tabindex="3" class="btn btn-inverse large" type="submit" value="Sign In">
									 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                                        <hr>
									<p class="reset">Recover your <a tabindex="4" href="#" title="Recover your username or password">username or password</a></p>
								</div>
							</fieldset>
						</form>	
                                                                         <div style="color:red">
                            <c:if test="${not empty error}">  
                                ${error}                                                   
                            </c:if>
                        </div>
                        <div style="color:green">
                            <c:if test="${not empty filesuccess}">  
                                ${filesuccess}                                                   
                            </c:if>
                            <c:if test="${not empty msg}">  
                                ${msg}                                                   
                            </c:if>
                        </div>
					</div>
					<div class="span7">					
						<h4 class="title"><span class="text"><strong>Register</strong> Form</span></h4>
						<form action="SubmitSignup?${_csrf.parameterName}=${_csrf.token}" method="post" class="form-stacked">
							<fieldset>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
										<input type="text" name="username" placeholder="Enter your username" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Email address:</label>
									<div class="controls">
										<input type="email" name="email" placeholder="Enter your email" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Password:</label>
									<div class="controls">
										<input type="password" name="password" placeholder="Enter your password" class="input-xlarge">
									</div>
								</div>	
                                                            <div class="control-group">
									<label class="control-label">Confirm Password:</label>
									<div class="controls">
										<input type="password" name="repassword" placeholder="Enter your password" class="input-xlarge">
									</div>
								</div>	
                                                            <div class="control-group">
									<label class="control-label">Address</label>
									<div class="controls">
										<input type="text" name="address" placeholder="Enter your password" class="input-xlarge">
									</div>
								</div>	
                                                             <div class="control-group">
                                                        <select class="controls" name="gender">
                                                            <option>Gender</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Other</option>
                                                        </select></div>
								<div class="control-group">
									<p>Now that we know who you are. I'm not a mistake! In a comic, you know how you can tell who the arch-villain's going to be?</p>
								</div>
								<hr>
								<div class="actions"><input tabindex="9" class="btn btn-inverse large" type="submit" value="Create your account"></div>
							 <!--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>-->
                                                        </fieldset>
						</form>					
					</div>				
				</div>
			</section>			
		<%@include file="Footerproperty.jsp" %>
			
    </body>
</html>