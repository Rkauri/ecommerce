 <div id="header-topbar-option-demo" class="page-header-topbar">
            <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" class="navbar navbar-default navbar-static-top">
           
            <div class="topbar-main"><a id="menu-toggle" href="#" class="hidden-xs"><i class="fa fa-bars"></i></a>
                
                <form id="topbar-search" action="" method="" class="hidden-sm hidden-xs">
                    <div class="input-icon right text-white"><a href="#"><i class="fa fa-search"></i></a><input type="text" placeholder="Search here..." class="form-control text-white"/></div>
                </form>
                <div class="news-update-box hidden-xs"><span class="text-uppercase mrm pull-left text-white">News:</span>
                    <ul id="news-update" class="ticker list-unstyled">
                        <li>Welcome to KAdmin - Responsive Multi-Style Admin Template</li>
                        <li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</li>
                    </ul>
                </div>
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                   
                    <li class="dropdown topbar-user"><a data-hover="dropdown" href="#" class="dropdown-toggle"><img src="images/avatar/48.jpg" alt="" class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">Welcome: ${pageContext.request.userPrincipal.name}</span>&nbsp;<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                            <li><a href="#"><i class="fa fa-user"></i>My Profile</a></li>
                           <c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}?${_csrf.parameterName}=${_csrf.token}" method="post" id="logoutForm">
<!--		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />-->
	</form>
<!--                            <li><a href="javascript:formSubmit()"><i class="fa fa-key"></i>Log Out</a></li>-->
<li><a href="javascript:formSubmit()"><i class="fa fa-key"></i>Log Out</a></li>
                        </ul>
                    </li>
                    </ul>
            </div>
        </nav>
            <!--BEGIN MODAL CONFIG PORTLET-->
          
            <!--END MODAL CONFIG PORTLET-->
        </div>